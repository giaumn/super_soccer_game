extends Control

var title = 'Ops! Error' setget _set_title
var message = 'Oops! Something went wrong. Try again later!' setget _set_message

func _init():
	add_user_signal(Constant.SIGNAL_MODAL_CLOSE)
	
func _ready():
	$Center/Title.text = title
	$Center/Message.text = message

func _set_title(value):
	title = value
	$Center/Title.text = message
	
func _set_message(value):
	message = value
	$Center/Message.text = message

func close():
	if self.is_inside_tree():
		get_tree().get_root().remove_child(self)
		emit_signal(Constant.SIGNAL_MODAL_CLOSE)

func _on_Button_button_up():
	close()
