extends Control

var title = 'Confirm?' setget _set_title
var message = 'Confirm?' setget _set_message

var _result = 'cancel'

func _init():
	add_user_signal(Constant.SIGNAL_MODAL_CLOSE)
	
func _ready():
	$Center/Title.text = title
	$Center/Message.text = message

func _set_title(value):
	title = value
	$Center/Title.text = message
	
func _set_message(value):
	message = value
	$Center/Message.text = message

func close():
	if self.is_inside_tree():
		get_tree().get_root().remove_child(self)
		emit_signal(Constant.SIGNAL_MODAL_CLOSE, _result)

func _on_Left_button_up():
	_result = 'override'
	close()

func _on_Center_button_up():
	_result = 'restore'
	close()

func _on_Right_button_up():
	_result = 'cancel'
	close()
