extends HBoxContainer

func init(position, data):
	$Position.text = var2str(position)
	$TeamName.text = data.get('team_name')
	$MP.text = var2str(int(data.get('match_played')))
	$Won.text = var2str(int(data.get('won')))
	$Draw.text = var2str(int(data.get('draw')))
	$Loss.text = var2str(int(data.get('loss')))
	$GF.text = var2str(int(data.get('goals_for')))
	$GA.text = var2str(int(data.get('goals_against')))
	$GD.text = var2str(int(data.get('goals_difference')))
	$Points.text = var2str(int(data.get('points')))

	if position <= 4:
		$Position.add_color_override('font_color', Color('33fa32'))
		$TeamName.add_color_override('font_color', Color('33fa32'))
		$MP.add_color_override('font_color', Color('33fa32'))
		$Won.add_color_override('font_color', Color('33fa32'))
		$Draw.add_color_override('font_color', Color('33fa32'))
		$Loss.add_color_override('font_color', Color('33fa32'))
		$GF.add_color_override('font_color', Color('33fa32'))
		$GA.add_color_override('font_color', Color('33fa32'))
		$GD.add_color_override('font_color', Color('33fa32'))
		$Points.add_color_override('font_color', Color('33fa32'))
	
	if position >= 21:
		$Position.add_color_override('font_color', Color('f52828'))
		$TeamName.add_color_override('font_color', Color('f52828'))
		$MP.add_color_override('font_color', Color('f52828'))
		$Won.add_color_override('font_color', Color('f52828'))
		$Draw.add_color_override('font_color', Color('f52828'))
		$Loss.add_color_override('font_color', Color('f52828'))
		$GF.add_color_override('font_color', Color('f52828'))
		$GA.add_color_override('font_color', Color('f52828'))
		$GD.add_color_override('font_color', Color('f52828'))
		$Points.add_color_override('font_color', Color('f52828'))
