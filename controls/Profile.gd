extends Control

func _init():
	add_user_signal(Constant.SIGNAL_USER_UPDATED_SUCCESS)
	
func _ready():
	if OS.get_name() == Constant.OS_ANDROID:
		if GPGS.is_signed_in() && Local.provider == Constant.PROVIDER_GUEST:
			# signed out of game play services if somehow previous was not successfully linked, and game services account is still signed in
			GPGS.sign_out()
		
		if Local.provider == Constant.PROVIDER_GOOGLE_PLAY_GAME_SERVICES:
			$Center/ConnectGoogle/Label.text = 'Linked as\n' + Local.email
			$Center/ConnectGoogle.disabled = true
			$Center/SignOut.show()
		elif Local.provider == Constant.PROVIDER_GUEST:
			$Center/ConnectGoogle/Label.text = 'LINK TO GOOGLE PLAY\nGAME SERVICES'
			$Center/ConnectGoogle.disabled = false
			$Center/SignOut.hide()
		$Center/ConnectGoogle.show()
	elif OS.get_name() == Constant.OS_IOS:
		$Center/ConnectGoogle.hide()
	else:
		$Center/ConnectGoogle.hide()
		$Center/SignOut.hide()

func _on_Close_button_up():
	close()

func init(_team_name, _coach_name):
	$Center/TeamName.text = _team_name
	$Center/CoachName.text = _coach_name

func close():
	get_parent().remove_child(self)

func _on_Save_button_up():
	var team_name = $Center/TeamName.text
	var coach_name = $Center/CoachName.text
	User.update({
		'team_name': team_name,
		'coach_name': coach_name
	})
	var response = yield(User, Constant.SIGNAL_USER_UPDATED)
	if response['success']:
		emit_signal(Constant.SIGNAL_USER_UPDATED_SUCCESS, response['data'])
		close()
	else:
		$Center/Error.text = response['messages'][0]

func _on_ConnectGoogle_button_up():
	Loader.show(self)
	
	GPGS.sign_in()
	var data = yield(GPGS, Constant.SIGNAL_GAME_SERVICES_SIGNED_IN)
	var id_token = data['token']
	User.link(Constant.PROVIDER_GOOGLE_PLAY_GAME_SERVICES, id_token, null)
	var response = yield(User, Constant.SIGNAL_USER_LINKED)
	
	Loader.hide(self)
	
	if response['success']:
		var id = response['data']['id']
		if id == Local.user_id:
			# found a user who is linked before but has the same id, this case is likely never happend
			Local.provider = response['data']['provider']
			Local.email = response['data']['email']
			_update(response, null)
		else:
			# found a different user who is linked before, ask user for override
			var option_modal = Modal.option('Override?', 'Old game account was found.\nChoose [Override] to override current data.\nChoose [Restore] to restore old game data.')
			var result = yield(option_modal, Constant.SIGNAL_MODAL_CLOSE)
			if result == 'override':
				var confirm_modal = Modal.confirm('Override?', 'Old game account will be permanently lost. Process?')
				if yield(confirm_modal, Constant.SIGNAL_MODAL_CLOSE):
					Loader.show(self)
					User.link(Constant.PROVIDER_GOOGLE_PLAY_GAME_SERVICES, id_token, result)
					response = yield(User, Constant.SIGNAL_USER_LINKED)
					Loader.hide(self)
					_update(response, result)
			elif result == 'restore':
				var confirm_modal = Modal.confirm('Restore?', 'Old game account will be restored on this device, current data will be lost. Process?')
				if yield(confirm_modal, Constant.SIGNAL_MODAL_CLOSE):
					Loader.show(self)
					User.link(Constant.PROVIDER_GOOGLE_PLAY_GAME_SERVICES, id_token, result)
					response = yield(User, Constant.SIGNAL_USER_LINKED)
					Loader.hide(self)
					_update(response, result)
			else:
				GPGS.sign_out()
	else:
		Modal.error(response['messages'][0])

func _on_SignOut_button_up():
	var confirm_modal = Modal.confirm('Sign out?', 'You will play as a guest after sign out, still process?')
	yield(confirm_modal, Constant.SIGNAL_MODAL_YES)
	GPGS.sign_out()
	Server.destroy_connection()
	Storage.save({'token': null})
	get_tree().reload_current_scene()
	
func _update(response, action):
	if response['success']:
		Local.provider = response['data']['provider']
		Local.user_id = response['data']['id']
		Local.email = response['data']['email']
		if action == 'override':
			if Local.provider == Constant.PROVIDER_GOOGLE_PLAY_GAME_SERVICES:
				$Center/ConnectGoogle/Label.text = 'Linked as\n' + Local.email
				$Center/ConnectGoogle.disabled = true
				$Center/SignOut.show()
			else:
				pass
		elif action == 'restore':
			var token = response['data']['token_type'] + ' ' + response['data']['token_value']
			Server.destroy_connection()
			Storage.save({'token': token})
			get_tree().reload_current_scene()
		else:
			$Center/ConnectGoogle/Label.text = 'Linked as\n' + Local.email
			$Center/ConnectGoogle.disabled = true
			$Center/SignOut.show()
	else:
		Modal.error(response['messages'][0])
