extends VBoxContainer

var LeagueStanding = preload('res://controls/LeagueStanding.tscn')

func init(data):
	var position = 1
	for row in data:
		var league_standing = LeagueStanding.instance()
		league_standing.init(position, row)
		add_child(league_standing)
		position += 1
