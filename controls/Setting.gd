extends Control

func _ready():
	var _music_volume = Storage.get_item('music_volume')
	var _sound_volume = Storage.get_item('sound_volume')
	
	_music_volume = _music_volume if _music_volume != null else Constant.MUSIC_VOLUME_DEFAULT
	_sound_volume = _sound_volume if _sound_volume != null else Constant.SOUND_VOLUME_DEFAULT
	
	$Center/MusicSlider.value = _music_volume
	$Center/SoundSlider.value = _sound_volume

func _on_MusicSlider_value_changed(value):
	Storage.set_item('music_volume', value)
	FBA.logEvent('ChangeMusic')
	Music.change_volume(value)

func _on_Close_button_up():
	get_parent().remove_child(self)

func _on_SoundSlider_value_changed(value):
	Storage.set_item('sound_volume', value)
	FBA.logEvent('ChangeSound')
	Sound.change_volume(value)
