extends TextureRect

class_name FormationPicker

var _selected_index = 10

var _formations = [
	'2-2-6',
	'2-3-5',
	'2-4-4',
	'2-5-3',
	'2-6-2',
	'3-2-5',
	'3-3-4',
	'3-4-3',
	'3-5-2',
	'4-2-4',
	'4-3-3',
	'4-4-2',
	'5-2-3',
	'5-3-2',
	'6-2-2',
]

func _init():
	add_user_signal(Constant.SIGNAL_FORMATION_CHANGED)

func _ready():
	$Formation.text = _formations[_selected_index]

func _on_Right_button_up():
	_selected_index += 1
	
	if _selected_index >= _formations.size():
		_selected_index = _formations.size() - 1
		
	$Formation.text = _formations[_selected_index]
	emit_signal(Constant.SIGNAL_FORMATION_CHANGED, _formations[_selected_index])

func _on_Left_button_up():
	_selected_index -= 1
	
	if _selected_index < 0:
		_selected_index = 0
		
	$Formation.text = _formations[_selected_index]
	emit_signal(Constant.SIGNAL_FORMATION_CHANGED, _formations[_selected_index])

func disable():
	$Left.disabled = true
	$Right.disabled = true
	
func enable():
	$Left.disabled = false
	$Right.disabled = false

func set_formation(formation):
	var new_index = _formations.find(formation)
	if new_index == -1:
		return
		
	_selected_index = new_index
	$Formation.text = _formations[_selected_index]
