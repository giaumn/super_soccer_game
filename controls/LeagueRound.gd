extends Control

var LeagueMatch = preload('res://controls/LeagueMatch.tscn')

func init(r, current_round_number):
	var start_date = r.get('formatted_start_date')
	var start_time = r.get('start_time')
	var pairs = r.get('pairs')
	var round_number = r.get('round_number')
	
	var original = r.get('original')
	if original == null:
		$Header/Subtitle.text = 'Official Round'
	else:
		$Header/Subtitle.text = 'Extended of Round ' + var2str(int(original.get('round_number')))
	
	$Header/Title/Round.text = 'Round ' + var2str(int(round_number))
	$Header/Title/Date.text = start_date
	
	var now = OS.get_time()
	var start_hour = int(start_time.split(':')[0])
	var start_minute = int(start_time.split(':')[1])
	
	if round_number < current_round_number:
		$Header/Title/Round.add_color_override('font_color', Color('525252'))
	elif round_number == current_round_number:
		$Header/Title/Round.add_color_override('font_color', Color('edca0a'))
	else:
		$Header/Title/Round.add_color_override('font_color', Color('ffffff'))
	
	if start_date == 'Today':
		if now.hour >= start_hour && now.minute >= start_minute:
			$Header/Title/Right/Start.hide()
			$Header/Title/Right/Countdown.show()
			$Header/Title/Right/Countdown.text = 'Kicked off'
		else:
			$Header/Title/Right/Start.hide()
			$Header/Title/Right/Countdown.show()
			var minutes = (start_hour * 60 + start_minute) - (now.hour * 60 + now.minute) 
			$Header/Title/Right/Countdown.text = 'Kick off in ' + '%02d hours and %02d minutes' % [minutes / 60, minutes % 60]
	else:
		$Header/Title/Right/Start.hide()
		$Header/Title/Right/Countdown.hide()
	
	for pair in pairs:
		var round_match = LeagueMatch.instance()
		var home = pair.get('home')
		var away = pair.get('away')
		var match_data = pair.get('match')
		round_match.init(home, away, match_data, start_time)
		$Container.add_child(round_match)
		
