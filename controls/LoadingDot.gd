extends Label

var _timer = Timer.new()
var _dot = ''
var _initial_text = ''

func _ready():
	_initial_text = self.text
	_timer.connect('timeout', self, '_on_timer_timout')
	_timer.one_shot = false
	_timer.wait_time = 0.25
	_timer.autostart = true
	add_child(_timer)

func _on_timer_timout():
	if _dot == '':
		self.text = _initial_text + '.'
		_dot += '.'
	elif _dot == '.':
		self.text = _initial_text + '..'
		_dot += '.'
	elif _dot == '..':
		self.text = _initial_text + '...'
		_dot += '.'
	else:
		self.text = _initial_text
		_dot = ''
