extends TextureRect

var _selected = false
var _opponent_selected = false
var _to_player

var side
var number
var is_having_ball

func _init():
	add_user_signal(Constant.SIGNAL_GAME_PLAYER_SELECTED)
	add_user_signal(Constant.SIGNAL_GAME_PLAYER_PASSED_BALL)
	
func _ready():
	self.connect(Constant.SIGNAL_GAME_PLAYER_PASSED_BALL, self, '_on_ball_passed')

func _calculate_angle(p1x, p1y, p2x, p2y, p3x, p3y):
	var numerator = p2y*(p1x-p3x) + p1y*(p3x-p2x) + p3y*(p2x-p1x)
	var denominator = (p2x-p1x)*(p1x-p3x) + (p2y-p1y)*(p1y-p3y);
	var ratio = numerator / denominator;

	var angle_rad = atan(ratio);
	var angle_deg = (angle_rad * 180) / PI;

	return angle_deg;

func init(_side, _number, _is_having_ball):
	side = _side
	number = _number
	is_having_ball = _is_having_ball
	$Button/Number.text = '%02d' % [number]
	if side == Constant.ME:
		$Button.texture_normal = load('res://assets/textures/player_green.png')
	else:
		$Button.texture_normal = load('res://assets/textures/player_red.png')
	
	var font = $Button/Number.get_font('font')
	var unique_font = font.duplicate()
	$Button/Number.add_font_override('font', unique_font)
		
	if is_having_ball:
		$Ball.show()
	else:
		$Ball.hide()
		
	if is_having_ball:
		if side == Constant.ME:
			$Forward.show()
			$Backward.hide()
		else:
			$Forward.hide()
			$Backward.show()
	else:
		$Forward.hide()
		$Backward.hide()
		
	$Opponent.hide()
	$Me.hide()

func highlight():
	if side == Constant.ME:
		$CircleGreen.show()
		$CircleRed.hide()
	else:
		$CircleGreen.hide()
		$CircleRed.show()
	
func normalize():
	$CircleGreen.hide()
	$CircleRed.hide()

func select(from: Control, ball_control: Control, change_direction):
	if from != null && from.number != self.number:
		from.deselect()
					
	if _selected:
		return
		
	_selected = true
	
	if change_direction:
		# change direction of the arrow
		var rotate_degree = _calculate_angle(
			ball_control.rect_position.x, ball_control.rect_position.y,
			self.rect_position.x, self.rect_position.y,
			ball_control.rect_position.x, ball_control.rect_position.y - 300
		)
		ball_control.change_arrow_rotate_degree(rotate_degree)
	$Me.show()
	$Me/Label.text = Local.me.get('user_name')

func deselect():
	if _selected:
		_selected = false
	
	$Me.hide()
		
func lock():
	if !_selected:
		return
	$Me/Lock.texture = preload('res://assets/textures/ic_lock.png')
		
func change_arrow_rotate_degree(degree):
	if side == Constant.ME:
		$Forward.rect_rotation = degree
	else:
		$Backward.rect_rotation = degree

func opponent_select(previous):
	if previous != null && previous.number != self.number:
		previous.opponent_deselect()
		
	if _opponent_selected:
		return
		
	_opponent_selected = true
	$Opponent.show()
	$Opponent/Label.text = Local.opponent.get('user_name')
		
func opponent_lock():
	if !_opponent_selected:
		return
	$Opponent/Lock.texture = preload('res://assets/textures/ic_lock.png')

func opponent_deselect():
	if _opponent_selected:
		_opponent_selected = false
	$Opponent.hide()

func pass_ball(to_player: Control):
	Sound.play_second('pass.wav')
	
	_to_player = to_player
	
	# ball controlling player pass the ball away
	var target_x = _to_player.rect_position.x - self.rect_position.x + $Ball.rect_position.x
	var target_y = _to_player.rect_position.y - self.rect_position.y + $Ball.rect_position.y
	
	var animation: Animation = $Ball/AnimationPlayer.get_animation('move')
	animation.track_set_key_value(0, 1, [target_x, -0.25, 0, 0.25, 0])
	animation.track_set_key_value(1, 1, [target_y, -0.25, 0, 0.25, 0])
	$Ball/AnimationPlayer.play('move')
	
func receive_ball():
	$Ball.show()

func _on_Button_button_up():
	emit_signal(Constant.SIGNAL_GAME_PLAYER_SELECTED, self)

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'move':
		emit_signal(Constant.SIGNAL_GAME_PLAYER_PASSED_BALL)

func _on_ball_passed():
	$Ball.hide()
	$Ball/AnimationPlayer.play('RESET')
	_to_player.receive_ball()
