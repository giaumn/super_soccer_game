extends Control

var country

func _init():
	add_user_signal(Constant.SIGNAL_COUNTRY_SELECTED)

func init(_country):
	country = _country
	$Label.text = country.get('name')

func _on_Container_pressed():
	emit_signal(Constant.SIGNAL_COUNTRY_SELECTED, country)
	FBA.logEvent('Choose ' + country.get('name'))
