extends HBoxContainer

var _home
var _away
var _match_data
var _time

func _ready():
	var home_id = _home.get('id')
	var away_id = _away.get('id')
	if home_id == Local.user_id:
		$Left/Home.add_color_override('font_color', Color('edca0a'))
	elif away_id == Local.user_id:
		$Left/Away.add_color_override('font_color', Color('edca0a'))
	$Left/Home.text = _home.get('team_name')
	$Left/Away.text = _away.get('team_name')
	$Right/Time.text = _time
	
	if _match_data == null:
		$Center/Home.text = ''
		$Center/Away.text = ''
	else:
	
		var home_score = _match_data.get('home_score')
		var away_score = _match_data.get('away_score')
		
		$Center/Home.text = var2str(int(home_score))
		$Center/Away.text = var2str(int(away_score))
		
		if home_score > away_score:
			$Center/Home.add_color_override('font_color', Color('edca0a'))
		elif away_score > home_score:
			$Center/Away.add_color_override('font_color', Color('edca0a'))

func init(home, away, match_data, time):
	_home = home
	_away = away
	_match_data = match_data
	_time = time
