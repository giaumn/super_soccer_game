extends TextureRect

var _selection: Control
var action
var strike

func _init():
	add_user_signal(Constant.SIGNAL_GAME_SHOOTER_SELECTED)

func init(_action):
	action = _action
	if action == Constant.ATTACK:
		$Attack.show()
		$Defend.hide()
	elif action == Constant.DEFEND:
		$Attack.hide()
		$Defend.show()
	else:
		$Attack.hide()
		$Defend.hide()
		
func lock():
	_selection.texture = preload('res://assets/textures/ic_lock.png')
	
func shoot(attack_selection, defend_selection):
	$Forward.hide()
	$Attack.hide()
	$Defend.hide()
	
	$Ball/AnimationPlayer.play('strike_' + attack_selection)
	if attack_selection == defend_selection:
		Sound.play_second('shoot.wav')
		$Goal/GoalKeeper/AnimationPlayer.play('move_' + defend_selection)
	else:
		Sound.play_second('shoot_no_ball.wav')
		$Goal/GoalKeeper/AnimationPlayer.play('move_' + defend_selection + '_no_ball')

func _on_Left_button_up():
	if _selection != null:
		_selection.hide()
	
	if action == Constant.ATTACK:
		$Attack/Left/Lock.show()
		_selection = $Attack/Left/Lock
		$Forward.rect_rotation = -25
	elif action == Constant.DEFEND:
		$Defend/Left/Lock.show()
		_selection = $Defend/Left/Lock
		$Goal/GoalKeeper.rect_position.x = 15
		
	emit_signal(Constant.SIGNAL_GAME_SHOOTER_SELECTED, 'left')

func _on_Center_button_up():
	if _selection != null:
		_selection.hide()
	
	if action == Constant.ATTACK:
		$Attack/Center/Lock.show()
		_selection = $Attack/Center/Lock
		$Forward.rect_rotation = 0
	elif action == Constant.DEFEND:
		$Defend/Center/Lock.show()
		_selection = $Defend/Center/Lock
		$Goal/GoalKeeper.rect_position.x = 134
	
	emit_signal(Constant.SIGNAL_GAME_SHOOTER_SELECTED, 'center')

func _on_Right_button_up():
	if _selection != null:
		_selection.hide()

	if action == Constant.ATTACK:
		$Attack/Right/Lock.show()
		_selection = $Attack/Right/Lock
		$Forward.rect_rotation = 25
	elif action == Constant.DEFEND:
		$Defend/Right/Lock.show()
		_selection = $Defend/Right/Lock
		$Goal/GoalKeeper.rect_position.x = 240
	
	emit_signal(Constant.SIGNAL_GAME_SHOOTER_SELECTED, 'right')

func _on_AnimationPlayer_animation_finished(anim_name):
	$Ball.hide()
	$Ball/AnimationPlayer.play('RESET')
