extends TextureRect

var rank
var team_name
var coach_name
var country_code
var points

func init(_rank, _team_name, _coach_name, _country_code, _points):
	rank = _rank
	team_name = _team_name
	coach_name = _coach_name
	points = _points
	country_code = _country_code
	
	$Rank.text = '%02d' % [rank]
	if rank == 1:
		$Rank.add_color_override('font_color', Color('#f1be0c'))
	elif rank == 2:
		$Rank.add_color_override('font_color', Color('#eb5353'))
	elif rank == 3:
		$Rank.add_color_override('font_color', Color('#b87333'))
		
	$Team.text = 'FC. ' + team_name
	$Coach.text = coach_name
	$Flag.texture = load('res://assets/textures/w40/' + _country_code + '.png')
	$Points.text = var2str(points)
