extends Node

var token = null
var provider = null
var user_id = null
var user_name = ''
var team_name = ''
var country_code = ''
var email = ''
var formation = null
var league = null

var current_match = null
var me = null
var opponent = null
var home = null
var away = null
var wait_secs_before_started = null
var requesting = false
var reconnected = false
var last_cancelation = null
