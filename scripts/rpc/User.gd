extends Node

func _init():
	add_user_signal(Constant.SIGNAL_HTTP_DATA)
	add_user_signal(Constant.SIGNAL_USER_HAD_INFO)
	add_user_signal(Constant.SIGNAL_USER_UPDATED)
	add_user_signal(Constant.SIGNAL_USER_LINKED)

func _ready():
	Server.connect(Constant.SIGNAL_USER_AUTHENTICATE, self, '_on_user_authenticate')

func _on_user_authenticate():
	# try to get user ip address
	HttpCaller.connect(Constant.SIGNAL_HTTP_SUCCESS, self, '_on_get_ip_address_success', [], 8)
	HttpCaller.connect(Constant.SIGNAL_HTTP_ERROR, self, '_on_get_ip_address_error', [], 8)
	HttpCaller.get('https://api.ipify.org/?format=json')

func _on_get_ip_address_success(response):
	var ip_address = response['ip']
	var token = Storage.get_item('token')
	if token != null:
		Local.token = token
	rpc_id(1, Constant.RPC_USER_AUTHENTICATE, {'token': token, 'ip_address': ip_address}) # 1 is the id of the server

func _on_get_ip_address_error():
	Server.notify_error(0, 'Oops! Something went wrong. Please try again!')
	yield(get_tree().create_timer(2.0), 'timeout')
	Server.destroy_connection()
	get_tree().reload_current_scene()
	
func fetch():
	if Local.token == null:
		return
	# fetch user info after authenticate
	rpc_id(1, Constant.RPC_USER_FETCH, {'token': Local.token})
	
remote func ret_authenticate(response):
	var token = response['token_type'] + ' ' + response['token_value']
	var data = {'token': token}
	Storage.save(data)
	Local.token = token
	# fetch user info after authenticate
	rpc_id(1, Constant.RPC_USER_FETCH, {'token': token})
	
remote func ret_fetch(response):
	Local.provider = response['provider']
	Local.email = response['email']
	Local.team_name = response['team_name']
	Local.user_name = response['name']
	Local.country_code = response['country_code']
	Local.user_id = response['id']
	Local.formation = '4-3-3' if response['preferred_formation'] == null else response['preferred_formation']
	Local.last_cancelation = response['last_cancelation']
	emit_signal(Constant.SIGNAL_USER_HAD_INFO, response)

func update(data):
	var team_name = data.get('team_name')
	var coach_name = data.get('coach_name')
	var fcm_token = data.get('fcm_token')
	rpc_id(1, Constant.RPC_USER_UPDATE, {
		'token': Local.token,
		'team_name': team_name,
		'coach_name': coach_name,
		'fcm_token': fcm_token
	})

remote func ret_update(response):
	emit_signal(Constant.SIGNAL_USER_UPDATED, response)

func link(provider, id_token, action):
	rpc_id(1, Constant.RPC_USER_LINK, {'token': Local.token, 'provider': provider, 'id_token': id_token, 'action': action})

remote func ret_link(response):
	emit_signal(Constant.SIGNAL_USER_LINKED, response)
	
func fetch_national_league():
	rpc_id(1, Constant.RPC_USER_FETCH_NAIONAL_LEAGUE, {'token': Local.token})
	
remote func ret_fetch_national_league(response):
	emit_signal(Constant.SIGNAL_HTTP_DATA, response)
