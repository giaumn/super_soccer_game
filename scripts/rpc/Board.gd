extends Node

func _init():
	add_user_signal(Constant.SIGNAL_BOARD_FETCHED)

func fetch(country_code, page = 1, per = Constant.ITEMS_PER_PAGE):
	rpc_id(1, Constant.RPC_BOARD_FETCH, {'token': Local.token, 'country_code': country_code, 'page': page, 'per': per})
	
remote func ret_fetch(data, paging):
	emit_signal(Constant.SIGNAL_BOARD_FETCHED, data, paging)
