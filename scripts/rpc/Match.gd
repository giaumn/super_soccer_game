extends Node

func _init():
	add_user_signal(Constant.SIGNAL_MATCH_FOUND)
	add_user_signal(Constant.SIGNAL_MATCH_INITIALIZED)
	add_user_signal(Constant.SIGNAL_MATCH_STARTED)
	add_user_signal(Constant.SIGNAL_MATCH_CANCELED)
	add_user_signal(Constant.SIGNAL_MATCH_NEW_TURN)
	add_user_signal(Constant.SIGNAL_MATCH_SKIP_TURN)
	add_user_signal(Constant.SIGNAL_MATCH_NEW_ROUND)
	add_user_signal(Constant.SIGNAL_MATCH_END_ROUND)
	add_user_signal(Constant.SIGNAL_MATCH_SCORE)
	add_user_signal(Constant.SIGNAL_MATCH_SYNC)
	add_user_signal(Constant.SIGNAL_MATCH_BREAK)
	add_user_signal(Constant.SIGNAL_MATCH_RETURN)
	add_user_signal(Constant.SIGNAL_MATCH_END)
	add_user_signal(Constant.SIGNAL_MATCH_OPPONENT_SELECT)
	add_user_signal(Constant.SIGNAL_MATCH_OPPONENT_CONFIRM)
	
func request():
	rpc_id(1, Constant.RPC_MATCH_REQUEST, {'token': Local.token, 'user_id': Local.user_id})

func cancel_request():
	rpc_id(1, Constant.RPC_MATCH_REQUEST_CANCEL, {'token': Local.token, 'user_id': Local.user_id})

func confirm():
	rpc_id(1, Constant.RPC_MATCH_CONFIRM, {'token': Local.token, 'user_id': Local.user_id, 'formation': Local.formation})
	
func reject():
	rpc_id(1, Constant.RPC_MATCH_REJECT, {'token': Local.token, 'user_id': Local.user_id})
	
func reconnect():
	rpc_id(1, Constant.RPC_MATCH_RECONNECT, {'token': Local.token, 'user_id': Local.user_id})

func select(turn_count, selection):
	rpc_id(1, Constant.RPC_MATCH_SELECT, {'token': Local.token, 'user_id': Local.user_id, 'turn_count': turn_count, 'selection': selection})

func move(turn_count, selection):
	rpc_id(1, Constant.RPC_MATCH_MOVE, {'token': Local.token, 'user_id': Local.user_id, 'turn_count': turn_count, 'selection': selection})

func quit():
	rpc_id(1, Constant.RPC_MATCH_QUIT, {'token': Local.token, 'user_id': Local.user_id})
	
func change_formation(formation):
	rpc_id(1, Constant.RPC_MATCH_CHANGE_FORMATION, {'token': Local.token, 'user_id': Local.user_id, 'formation': formation})

remote func ret_request(home, away):
	emit_signal(Constant.SIGNAL_MATCH_FOUND, home, away)
	
remote func init(data):
	emit_signal(Constant.SIGNAL_MATCH_INITIALIZED, data)
	
remote func start(data):
	emit_signal(Constant.SIGNAL_MATCH_STARTED, data)

remote func cancel():
	emit_signal(Constant.SIGNAL_MATCH_CANCELED)

remote func new_turn(data):
	emit_signal(Constant.SIGNAL_MATCH_NEW_TURN, data)

remote func skip_turn(data):
	emit_signal(Constant.SIGNAL_MATCH_SKIP_TURN, data)
	
remote func new_round(data):
	emit_signal(Constant.SIGNAL_MATCH_NEW_ROUND, data)
	
remote func end_round(data):
	emit_signal(Constant.SIGNAL_MATCH_END_ROUND, data)
	
remote func score(data):
	emit_signal(Constant.SIGNAL_MATCH_SCORE, data)

remote func sync(data):
	emit_signal(Constant.SIGNAL_MATCH_SYNC, data)
	
remote func break(data):
	emit_signal(Constant.SIGNAL_MATCH_BREAK, data)
	
remote func return(data):
	emit_signal(Constant.SIGNAL_MATCH_RETURN, data)
	
remote func end(data):
	emit_signal(Constant.SIGNAL_MATCH_END, data)
	
remote func opponent_select(turn_count, selection):
	emit_signal(Constant.SIGNAL_MATCH_OPPONENT_SELECT, turn_count, selection)
	
remote func opponent_confirm(turn_count, selection):
	emit_signal(Constant.SIGNAL_MATCH_OPPONENT_CONFIRM, turn_count, selection)

remote func sync_cancel_time(canceled_time):
	Local.last_cancelation = canceled_time
