extends Node

var ErrorModal = preload('res://modals/Error.tscn')
var ConfirmModal = preload('res://modals/Confirm.tscn')
var OptionModal = preload('res://modals/Option.tscn')

func error(message):
	var _error_modal = ErrorModal.instance()
	_error_modal.message = message
	get_tree().get_root().add_child(_error_modal)
	return _error_modal

func confirm(title, message):
	var _confirm_modal = ConfirmModal.instance()
	_confirm_modal.title = title
	_confirm_modal.message = message
	get_tree().get_root().add_child(_confirm_modal)
	return _confirm_modal

func option(title, message):
	var _option_modal = OptionModal.instance()
	_option_modal.title = title
	_option_modal.message = message
	get_tree().get_root().add_child(_option_modal)
	return _option_modal
