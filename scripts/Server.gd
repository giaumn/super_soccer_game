extends Node

var SERVER_HOST = '192.168.1.5'
# var SERVER_HOST = 'server.escape30.com'
var SERVER_PORT = 7654
var peer = NetworkedMultiplayerENet.new()
var cert = load('res://certificates/public.crt')
var connected = false

var _status: int = 0
var _stream: StreamPeerTCP = StreamPeerTCP.new()
	
func _init():
	add_user_signal(Constant.SIGNAL_USER_AUTHENTICATE)
	add_user_signal(Constant.SIGNAL_ERROR)

func _player_connected(id):
	var network_id = get_tree().get_network_unique_id()
	if id == 1:
		print('Player connected: ', id, '-', network_id)
		# itself connected
		self.set_network_master(network_id)
		connected = true
		authenticate()	

func _player_disconnected(id):
	var network_id = get_tree().get_network_unique_id()
	if id == 1:
		# itself disconnected
		connected = false
		print('Player disconnected: ', id, '-', network_id)

func _server_disconnected():
	print('Server disconnected')
	connected = false
	emit_signal(Constant.SIGNAL_ERROR, ErrorCode.SERVER_DISCONNECTED, 'Disconnected from the server. Reconnecting...')
	_reconnect()

func _connected_fail():
	print('Server connection fail')
	connected = false
	_reconnect()

func create_connection():
	print('Create connection')
	peer.set_dtls_certificate(cert)
	peer.dtls_verify = false
	peer.use_dtls = true
	peer.create_client(SERVER_HOST, SERVER_PORT)
	get_tree().network_peer = peer
	get_tree().connect("connection_failed", self, "_connected_fail", [], 1)
	get_tree().connect("server_disconnected", self, "_server_disconnected", [], 1)
	get_tree().connect("network_peer_connected", self, "_player_connected", [], 1)
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected", [], 1)
	
func destroy_connection():
	print('Destroy connection')
	connected = false
	# destroy old connection
	get_tree().disconnect("connection_failed", self, "_connected_fail")
	get_tree().disconnect("server_disconnected", self, "_server_disconnected")
	get_tree().disconnect("network_peer_connected", self, "_player_connected")
	get_tree().disconnect("network_peer_disconnected", self, "_player_disconnected")
	peer.close_connection()
	get_tree().network_peer = null
	
func _reconnect():
	print('Server reconnecting...')
	destroy_connection()
	peer = NetworkedMultiplayerENet.new()
	create_connection()
	
func _notification(what):
	var os_name = OS.get_name()
	if os_name == Constant.OS_IOS or os_name == Constant.OS_ANDROID:
		match what:
			MainLoop.NOTIFICATION_WM_FOCUS_OUT:
				destroy_connection()
			MainLoop.NOTIFICATION_WM_FOCUS_IN:
				create_connection()
	
func authenticate():
	emit_signal(Constant.SIGNAL_USER_AUTHENTICATE)
	
func initialize():
	print('Connecting to server...')
	create_connection()

func notify_error(error_code, message):
	emit_signal(Constant.SIGNAL_ERROR, error_code, message)

remote func ret_error(error_code, message):
	emit_signal(Constant.SIGNAL_ERROR, error_code, message)
	if error_code >= 1000 && error_code < 2000:
		# Token error, remove it
		destroy_connection()
		Storage.save({'token': null})
		get_tree().reload_current_scene()
