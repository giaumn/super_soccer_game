extends Node

# var count = 1
# var FILE_PATH = 'res://' + var2str(count) + '/user.dat'
# var FILE_PATH = 'res://user.dat'
var FILE_PATH = 'user://user.dat'

var _data = {'token': null}

func save(data):
	var file = File.new()
	file.open(FILE_PATH, File.WRITE)
	file.store_string(var2str(data))
	file.close()

func get_item(key):
	var data = load_all()
	return data.get(key)

func set_item(key, value):
	var data = load_all()
	data[key] = value
	save(data)

func load_all():
	var file = File.new()
	
	if !file.file_exists(FILE_PATH):
		return _data
	
	file.open(FILE_PATH, File.READ)
	var read_data = str2var(file.get_as_text())
	
	if read_data is String and read_data == '':
		read_data = _data
	
	file.close()
	return read_data
