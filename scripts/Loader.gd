extends Node

var Spinner = preload('res://controls/Spinner.tscn')

var _spinner

func show(target):
	_spinner = Spinner.instance()
	target.add_child(_spinner)

func hide(target):
	target.remove_child(_spinner)
