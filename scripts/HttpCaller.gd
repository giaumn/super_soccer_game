extends Node

var _client = HTTPRequest.new()

func _init():
	add_user_signal(Constant.SIGNAL_HTTP_SUCCESS)
	add_user_signal(Constant.SIGNAL_HTTP_ERROR)

func _ready():
	add_child(_client)
	_client.connect('request_completed', self, '_on_http_request_completed')

# Called when the HTTP request is completed.
func _on_http_request_completed(_result, response_code, _headers, body):
	# print(_result, response_code, body.get_string_from_utf8())
	if response_code == 200:
		var response = parse_json(body.get_string_from_utf8())
		emit_signal(Constant.SIGNAL_HTTP_SUCCESS, response)
	else:
		emit_signal(Constant.SIGNAL_HTTP_ERROR)

func get(url):
	var error = _client.request(url)
	if error != OK:
		push_error('An error occurred in the HTTP request.')
