extends Node

func _init():
	FCM.connect('token_received', self, '_on_token_received')
	FCM.connect('message_received', self, '_on_message_received')

func _on_token_received(token: String) -> void:
	yield(User, Constant.SIGNAL_USER_HAD_INFO)
	User.update({
		'fcm_token': token
	})
	var response = yield(User, Constant.SIGNAL_USER_UPDATED)
	if response['success']:
		print('Update fcm token success')
	else:
		print('Cannot fcm token success')

func _on_message_received(message: Dictionary) -> void:
	pass
