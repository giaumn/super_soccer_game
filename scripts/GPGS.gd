extends Node

var _play_games_services

func _init():
	add_user_signal(Constant.SIGNAL_GAME_SERVICES_SIGNED_IN)

func _ready():
	var os_name = OS.get_name()
	if os_name == Constant.OS_ANDROID:
		# Check if plugin was added to the project
		if Engine.has_singleton('GodotPlayGamesServices'):
			_play_games_services = Engine.get_singleton('GodotPlayGamesServices')
			# Initialize plugin by calling init method and passing to it a boolean to enable/disable displaying game pop-ups

			var show_popups := true
			var request_email := true
			var request_profile := true
			var request_token = '884582076595-ft38j65kcld0itc7fgs7u4a6ilvoeqcd.apps.googleusercontent.com'
			_play_games_services.init(show_popups, request_email, request_profile, request_token)

			# Connect callbacks (Use only those that you need)
			_play_games_services.connect('_on_sign_in_success', self, '_on_sign_in_success') # account_id: String
			_play_games_services.connect('_on_sign_in_failed', self, '_on_sign_in_failed') # error_code: int
	elif os_name == Constant.OS_IOS:
		pass

func _on_sign_in_success(json_data: String):
	var user_profile = parse_json(json_data)
	if Server.connected:
		emit_signal(Constant.SIGNAL_GAME_SERVICES_SIGNED_IN, user_profile)
	else:
		Server.create_connection()
		yield(User, Constant.SIGNAL_USER_HAD_INFO)
		emit_signal(Constant.SIGNAL_GAME_SERVICES_SIGNED_IN, user_profile)
	
func _on_sign_in_failed(_error_code: int):
	print('Sign in failed. Error code: ' + var2str(_error_code))
	Modal.error('Sign in failed. Try again later!')
	
func sign_in():
	_play_games_services.signIn()

func sign_out():
	_play_games_services.signOut()

func is_signed_in():
	if _play_games_services == null: 
		return false
	return _play_games_services.isSignedIn()
