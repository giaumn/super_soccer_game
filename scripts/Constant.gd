extends Node

var OS_IOS = 'IOS'
var OS_ANDROID = 'Android'

var ME = 'me'
var OPPONENT = 'opponent'

var ATTACK = 'attack'
var IDLE = 'idle'
var DEFEND = 'defend'

var ITEMS_PER_PAGE = 20

var MUSIC_VOLUME_MIN = -30
var MUSIC_VOLUME_MAX = -10
var MUSIC_VOLUME_DEFAULT = -15

var SOUND_VOLUME_MIN = -20
var SOUND_VOLUME_MAX = 0
var SOUND_VOLUME_DEFAULT = -5

var STORAGE_KEY_LAST_CANCELATION = 'last_cancelation'

var PROVIDER_GUEST = 'guest'
var PROVIDER_GOOGLE_PLAY_GAME_SERVICES = 'google_play_game_services'

var RPC_USER_AUTHENTICATE = 'authenticate'
var RPC_USER_FETCH = 'fetch_user'
var RPC_USER_UPDATE = 'update'
var RPC_USER_LINK = 'link'
var RPC_USER_FETCH_NAIONAL_LEAGUE = 'fetch_national_league'

var RPC_MATCH_REQUEST = 'request'
var RPC_MATCH_REQUEST_CANCEL = 'cancel_request'
var RPC_MATCH_CONFIRM = 'confirm'
var RPC_MATCH_REJECT = 'reject'
var RPC_MATCH_RECONNECT = 'reconnect'
var RPC_MATCH_MOVE = 'move'
var RPC_MATCH_QUIT = 'quit'
var RPC_MATCH_SELECT = 'select'
var RPC_MATCH_CHANGE_FORMATION = 'change_formation'
var RPC_BOARD_FETCH = 'fetch'

var SIGNAL_HTTP_SUCCESS = 'signal_http_success'
var SIGNAL_HTTP_ERROR = 'signal_http_error'
var SIGNAL_HTTP_DATA = 'signal_http_data'

var SIGNAL_MODAL_CLOSE = 'signal_modal_close'
var SIGNAL_MODAL_YES = 'signal_modal_yes'
var SIGNAL_MODAL_NO = 'signal_modal_no'

var SIGNAL_ERROR = 'signal_error'
var SIGNAL_USER_AUTHENTICATE = 'signal_user_authenticate'
var SIGNAL_USER_HAD_INFO = 'signal_user_had_info'
var SIGNAL_USER_UPDATED = 'signal_user_updated'
var SIGNAL_USER_UPDATED_SUCCESS = 'signal_user_updated_success'
var SIGNAL_USER_LINKED = 'signal_user_linked'

var SIGNAL_MATCH_FOUND = 'signal_match_found'
var SIGNAL_MATCH_INITIALIZED = 'signal_match_initialized'
var SIGNAL_MATCH_STARTED = 'signal_match_started'
var SIGNAL_MATCH_CANCELED = 'signal_match_canceled'
var SIGNAL_MATCH_NEW_TURN = 'signal_match_new_turn'
var SIGNAL_MATCH_SKIP_TURN = 'signal_match_skip_turn'
var SIGNAL_MATCH_NEW_ROUND = 'signal_match_new_round'
var SIGNAL_MATCH_END_ROUND = 'signal_match_end_round'
var SIGNAL_MATCH_SCORE = 'signal_match_score'
var SIGNAL_MATCH_SYNC = 'signal_match_sync'
var SIGNAL_MATCH_BREAK = 'signal_match_break'
var SIGNAL_MATCH_RETURN = 'signal_match_return'
var SIGNAL_MATCH_END = 'signal_match_end'
var SIGNAL_MATCH_OPPONENT_SELECT = 'signal_match_opponent_select'
var SIGNAL_MATCH_OPPONENT_CONFIRM = 'signal_match_opponent_confirm'

var SIGNAL_GAME_PLAYER_SELECTED = 'signal_game_player_selected'
var SIGNAL_GAME_PLAYER_PASSED_BALL = 'signal_game_player_passed_away'
var SIGNAL_GAME_SHOOTER_SELECTED = 'signal_game_shooter_selected'

var SIGNAL_FORMATION_CHANGED = 'signal_formation_changed'
var SIGNAL_COUNTRY_SELECTED = 'signal_country_selected'

var SIGNAL_BOARD_FETCHED = 'signal_board_fetched'

var SIGNAL_GAME_SERVICES_SIGNED_IN = 'signal_game_services_signed_in'
