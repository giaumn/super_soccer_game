extends Base

var BoardItem = preload('res://controls/BoardItem.tscn')
var Country = preload('res://controls/Country.tscn')
var _countries = [
	{'name': 'Global League', 'code': 'global'},
	{'name':'Afghanistan','code':'AF'},
	{'name':'Aland Islands','code':'AX'},
	{'name':'Albania','code':'AL'},
	{'name':'Algeria','code':'DZ'},
	{'name':'American Samoa','code':'AS'},
	{'name':'Andorra','code':'AD'},
	{'name':'Angola','code':'AO'},
	{'name':'Anguilla','code':'AI'},
	{'name':'Antarctica','code':'AQ'},
	{'name':'Antigua and Barbuda','code':'AG'},
	{'name':'Argentina','code':'AR'},
	{'name':'Armenia','code':'AM'},
	{'name':'Aruba','code':'AW'},
	{'name':'Australia','code':'AU'},
	{'name':'Austria','code':'AT'},
	{'name':'Azerbaijan','code':'AZ'},
	{'name':'Bahamas','code':'BS'},
	{'name':'Bahrain','code':'BH'},
	{'name':'Bangladesh','code':'BD'},
	{'name':'Barbados','code':'BB'},
	{'name':'Belarus','code':'BY'},
	{'name':'Belgium','code':'BE'},
	{'name':'Belize','code':'BZ'},
	{'name':'Benin','code':'BJ'},
	{'name':'Bermuda','code':'BM'},
	{'name':'Bhutan','code':'BT'},
	{'name':'Bolivia','code':'BO'},
	{'name':'Bonaire, Sint Eustatius and Saba','code':'BQ'},
	{'name':'Bosnia and Herzegovina','code':'BA'},
	{'name':'Botswana','code':'BW'},
	{'name':'Bouvet Island','code':'BV'},
	{'name':'Brazil','code':'BR'},
	{'name':'British Indian Ocean Territory','code':'IO'},
	{'name':'Brunei Darussalam','code':'BN'},
	{'name':'Bulgaria','code':'BG'},
	{'name':'Burkina Faso','code':'BF'},
	{'name':'Burundi','code':'BI'},
	{'name':'Cambodia','code':'KH'},
	{'name':'Cameroon','code':'CM'},
	{'name':'Canada','code':'CA'},
	{'name':'Cape Verde','code':'CV'},
	{'name':'Cayman Islands','code':'KY'},
	{'name':'Central African Republic','code':'CF'},
	{'name':'Chad','code':'TD'},
	{'name':'Chile','code':'CL'},
	{'name':'China','code':'CN'},
	{'name':'Christmas Island','code':'CX'},
	{'name':'Cocos (Keeling) Islands','code':'CC'},
	{'name':'Colombia','code':'CO'},
	{'name':'Comoros','code':'KM'},
	{'name':'Congo','code':'CG'},
	{'name':'Congo, Democratic Republic of the Congo','code':'CD'},
	{'name':'Cook Islands','code':'CK'},
	{'name':'Costa Rica','code':'CR'},
	{'name':'Cote D\'Ivoire','code':'CI'},
	{'name':'Croatia','code':'HR'},
	{'name':'Cuba','code':'CU'},
	{'name':'Curacao','code':'CW'},
	{'name':'Cyprus','code':'CY'},
	{'name':'Czech Republic','code':'CZ'},
	{'name':'Denmark','code':'DK'},
	{'name':'Djibouti','code':'DJ'},
	{'name':'Dominica','code':'DM'},
	{'name':'Dominican Republic','code':'DO'},
	{'name':'Ecuador','code':'EC'},
	{'name':'Egypt','code':'EG'},
	{'name':'El Salvador','code':'SV'},
	{'name':'Equatorial Guinea','code':'GQ'},
	{'name':'Eritrea','code':'ER'},
	{'name':'Estonia','code':'EE'},
	{'name':'Ethiopia','code':'ET'},
	{'name':'Falkland Islands (Malvinas)','code':'FK'},
	{'name':'Faroe Islands','code':'FO'},
	{'name':'Fiji','code':'FJ'},
	{'name':'Finland','code':'FI'},
	{'name':'France','code':'FR'},
	{'name':'French Guiana','code':'GF'},
	{'name':'French Polynesia','code':'PF'},
	{'name':'French Southern Territories','code':'TF'},
	{'name':'Gabon','code':'GA'},
	{'name':'Gambia','code':'GM'},
	{'name':'Georgia','code':'GE'},
	{'name':'Germany','code':'DE'},
	{'name':'Ghana','code':'GH'},
	{'name':'Gibraltar','code':'GI'},
	{'name':'Greece','code':'GR'},
	{'name':'Greenland','code':'GL'},
	{'name':'Grenada','code':'GD'},
	{'name':'Guadeloupe','code':'GP'},
	{'name':'Guam','code':'GU'},
	{'name':'Guatemala','code':'GT'},
	{'name':'Guernsey','code':'GG'},
	{'name':'Guinea','code':'GN'},
	{'name':'Guinea-Bissau','code':'GW'},
	{'name':'Guyana','code':'GY'},
	{'name':'Haiti','code':'HT'},
	{'name':'Heard Island and Mcdonald Islands','code':'HM'},
	{'name':'Holy See (Vatican City State)','code':'VA'},
	{'name':'Honduras','code':'HN'},
	{'name':'Hong Kong','code':'HK'},
	{'name':'Hungary','code':'HU'},
	{'name':'Iceland','code':'IS'},
	{'name':'India','code':'IN'},
	{'name':'Indonesia','code':'ID'},
	{'name':'Iran, Islamic Republic of','code':'IR'},
	{'name':'Iraq','code':'IQ'},
	{'name':'Ireland','code':'IE'},
	{'name':'Isle of Man','code':'IM'},
	{'name':'Israel','code':'IL'},
	{'name':'Italy','code':'IT'},
	{'name':'Jamaica','code':'JM'},
	{'name':'Japan','code':'JP'},
	{'name':'Jersey','code':'JE'},
	{'name':'Jordan','code':'JO'},
	{'name':'Kazakhstan','code':'KZ'},
	{'name':'Kenya','code':'KE'},
	{'name':'Kiribati','code':'KI'},
	{'name':'Korea, Democratic People\'s Republic of','code':'KP'},
	{'name':'Korea, Republic of','code':'KR'},
	{'name':'Kosovo','code':'XK'},
	{'name':'Kuwait','code':'KW'},
	{'name':'Kyrgyzstan','code':'KG'},
	{'name':'Lao People\'s Democratic Republic','code':'LA'},
	{'name':'Latvia','code':'LV'},
	{'name':'Lebanon','code':'LB'},
	{'name':'Lesotho','code':'LS'},
	{'name':'Liberia','code':'LR'},
	{'name':'Libyan Arab Jamahiriya','code':'LY'},
	{'name':'Liechtenstein','code':'LI'},
	{'name':'Lithuania','code':'LT'},
	{'name':'Luxembourg','code':'LU'},
	{'name':'Macao','code':'MO'},
	{'name':'Macedonia, the Former Yugoslav Republic of','code':'MK'},
	{'name':'Madagascar','code':'MG'},
	{'name':'Malawi','code':'MW'},
	{'name':'Malaysia','code':'MY'},
	{'name':'Maldives','code':'MV'},
	{'name':'Mali','code':'ML'},
	{'name':'Malta','code':'MT'},
	{'name':'Marshall Islands','code':'MH'},
	{'name':'Martinique','code':'MQ'},
	{'name':'Mauritania','code':'MR'},
	{'name':'Mauritius','code':'MU'},
	{'name':'Mayotte','code':'YT'},
	{'name':'Mexico','code':'MX'},
	{'name':'Micronesia, Federated States of','code':'FM'},
	{'name':'Moldova, Republic of','code':'MD'},
	{'name':'Monaco','code':'MC'},
	{'name':'Mongolia','code':'MN'},
	{'name':'Montenegro','code':'ME'},
	{'name':'Montserrat','code':'MS'},
	{'name':'Morocco','code':'MA'},
	{'name':'Mozambique','code':'MZ'},
	{'name':'Myanmar','code':'MM'},
	{'name':'Namibia','code':'NA'},
	{'name':'Nauru','code':'NR'},
	{'name':'Nepal','code':'NP'},
	{'name':'Netherlands','code':'NL'},
	{'name':'Netherlands Antilles','code':'AN'},
	{'name':'New Caledonia','code':'NC'},
	{'name':'New Zealand','code':'NZ'},
	{'name':'Nicaragua','code':'NI'},
	{'name':'Niger','code':'NE'},
	{'name':'Nigeria','code':'NG'},
	{'name':'Niue','code':'NU'},
	{'name':'Norfolk Island','code':'NF'},
	{'name':'Northern Mariana Islands','code':'MP'},
	{'name':'Norway','code':'NO'},
	{'name':'Oman','code':'OM'},
	{'name':'Pakistan','code':'PK'},
	{'name':'Palau','code':'PW'},
	{'name':'Palestinian Territory, Occupied','code':'PS'},
	{'name':'Panama','code':'PA'},
	{'name':'Papua New Guinea','code':'PG'},
	{'name':'Paraguay','code':'PY'},
	{'name':'Peru','code':'PE'},
	{'name':'Philippines','code':'PH'},
	{'name':'Pitcairn','code':'PN'},
	{'name':'Poland','code':'PL'},
	{'name':'Portugal','code':'PT'},
	{'name':'Puerto Rico','code':'PR'},
	{'name':'Qatar','code':'QA'},
	{'name':'Reunion','code':'RE'},
	{'name':'Romania','code':'RO'},
	{'name':'Russian Federation','code':'RU'},
	{'name':'Rwanda','code':'RW'},
	{'name':'Saint Barthelemy','code':'BL'},
	{'name':'Saint Helena','code':'SH'},
	{'name':'Saint Kitts and Nevis','code':'KN'},
	{'name':'Saint Lucia','code':'LC'},
	{'name':'Saint Martin','code':'MF'},
	{'name':'Saint Pierre and Miquelon','code':'PM'},
	{'name':'Saint Vincent and the Grenadines','code':'VC'},
	{'name':'Samoa','code':'WS'},
	{'name':'San Marino','code':'SM'},
	{'name':'Sao Tome and Principe','code':'ST'},
	{'name':'Saudi Arabia','code':'SA'},
	{'name':'Senegal','code':'SN'},
	{'name':'Serbia','code':'RS'},
	{'name':'Serbia and Montenegro','code':'CS'},
	{'name':'Seychelles','code':'SC'},
	{'name':'Sierra Leone','code':'SL'},
	{'name':'Singapore','code':'SG'},
	{'name':'Sint Maarten','code':'SX'},
	{'name':'Slovakia','code':'SK'},
	{'name':'Slovenia','code':'SI'},
	{'name':'Solomon Islands','code':'SB'},
	{'name':'Somalia','code':'SO'},
	{'name':'South Africa','code':'ZA'},
	{'name':'South Georgia and the South Sandwich Islands','code':'GS'},
	{'name':'South Sudan','code':'SS'},
	{'name':'Spain','code':'ES'},
	{'name':'Sri Lanka','code':'LK'},
	{'name':'Sudan','code':'SD'},
	{'name':'Suriname','code':'SR'},
	{'name':'Svalbard and Jan Mayen','code':'SJ'},
	{'name':'Swaziland','code':'SZ'},
	{'name':'Sweden','code':'SE'},
	{'name':'Switzerland','code':'CH'},
	{'name':'Syrian Arab Republic','code':'SY'},
	{'name':'Taiwan, Province of China','code':'TW'},
	{'name':'Tajikistan','code':'TJ'},
	{'name':'Tanzania, United Republic of','code':'TZ'},
	{'name':'Thailand','code':'TH'},
	{'name':'Timor-Leste','code':'TL'},
	{'name':'Togo','code':'TG'},
	{'name':'Tokelau','code':'TK'},
	{'name':'Tonga','code':'TO'},
	{'name':'Trinidad and Tobago','code':'TT'},
	{'name':'Tunisia','code':'TN'},
	{'name':'Turkey','code':'TR'},
	{'name':'Turkmenistan','code':'TM'},
	{'name':'Turks and Caicos Islands','code':'TC'},
	{'name':'Tuvalu','code':'TV'},
	{'name':'Uganda','code':'UG'},
	{'name':'Ukraine','code':'UA'},
	{'name':'United Arab Emirates','code':'AE'},
	{'name':'United Kingdom','code':'GB'},
	{'name':'United States','code':'US'},
	{'name':'United States Minor Outlying Islands','code':'UM'},
	{'name':'Uruguay','code':'UY'},
	{'name':'Uzbekistan','code':'UZ'},
	{'name':'Vanuatu','code':'VU'},
	{'name':'Venezuela','code':'VE'},
	{'name':'Viet Nam','code':'VN'},
	{'name':'Virgin Islands, British','code':'VG'},
	{'name':'Virgin Islands, U.s.','code':'VI'},
	{'name':'Wallis and Futuna','code':'WF'},
	{'name':'Western Sahara','code':'EH'},
	{'name':'Yemen','code':'YE'},
	{'name':'Zambia','code':'ZM'},
	{'name':'Zimbabwe','code':'ZW'}
]

var _is_opened = false
var _page = 1
var _total_pages
var _country_code
var _is_loading = false

func _ready():
	$Center/Countries/Scrollable.get_v_scrollbar().rect_scale.x = 0
	$Center/ScrollContainer.get_v_scrollbar().rect_scale.x = 0
	$Center/ScrollContainer.rect_size.x = $Center.rect_size.x - 5
	$Center/Countries.hide()
	$Center/Close.hide()
		
	for country in _countries:
		var control = Country.instance()
		$Center/Countries/Scrollable/GridContainer.add_child(control)
		control.connect(Constant.SIGNAL_COUNTRY_SELECTED, self, '_on_country_selected')
		control.init(country)
	
	$Center/Loading.show()
	$Center/NoData.hide()
	$Center/MyRank.hide()
	_load_board(_countries[0])

func _process(_delta):
	var font_size = $Center/BoardName.get_font('font').get_string_size($Center/BoardName.text)
	if font_size.x > 500:
		$Center/BoardName.text = $Center/BoardName.text.substr(0, 18) + '...'
		
func _on_SelectCountry_button_up():
	if _is_opened:
		_is_opened = false
		$Center/Countries.hide()
		$Center/Close.hide()
	else:
		_is_opened = true
		$Center/Countries.show()
		$Center/Close.show()

func _on_Close_button_up():
	_is_opened = false
	$Center/Countries.hide()
	$Center/Close.hide()

func _on_Back_button_up():
	get_tree().change_scene('res://scenes/Main.tscn')
	
func _on_country_selected(country):
	_is_opened = false
	$Center/Countries.hide()
	$Center/Close.hide()
	_load_board(country)

func _load_board(country):
	_page = 1
	_country_code = country.get('code')
	$Center/BoardName.text = country.get('name')
	if $Center/ScrollContainer/VBoxContainer.get_child_count() > 0:
		Util.delete_children($Center/ScrollContainer/VBoxContainer)
		yield(get_tree().create_timer(0.2), 'timeout')
	$Center/NoData.hide()
	$Center/Loading.show()
	$Center/MyRank.hide()
	Board.connect(Constant.SIGNAL_BOARD_FETCHED, self, '_on_board_fetched')
	Board.fetch(_country_code, _page)

func _load_more():
	$Center/NoData.hide()
	$Center/Loading.show()
	Board.fetch(_country_code, _page)

func _on_board_fetched(data, paging):
	$Center/Loading.hide()
	var board = data.get('board')
	
	_total_pages = paging.get('total_pages')
	$Center/Total.text = 'Total ' + var2str(int(paging.get('total_count')))
	
	if board.size() == 0:
		$Center/NoData.show()
	else:
		var index = 0
		var latest_rank
		for item in board:
			var control = BoardItem.instance()
			$Center/ScrollContainer/VBoxContainer.add_child(control)
			latest_rank = (_page - 1) * Constant.ITEMS_PER_PAGE + (index + 1)
			control.init(latest_rank, item.get('team_name'), item.get('name'), item.get('country_code'), item.get('points'))
			yield(get_tree().create_timer(0.001), 'timeout')
			index += 1
		$Center/NoData.hide()
	
	var me = data.get('me')
	if me.get('country_code') == data.get('country_code') or data.get('country_code') == 'global':
		$Center/MyRank.text = 'You #' + var2str(int(me.get('rank')))
		$Center/MyRank.show()
	else:
		$Center/MyRank.hide()
	_is_loading = false

func _on_ScrollContainer_scroll_ended():
	var max_scroll = $Center/ScrollContainer/VBoxContainer.rect_size.y - $Center/ScrollContainer.rect_size.y
	# print(max_scroll, '---', $Center/ScrollContainer.scroll_vertical)
	if ($Center/ScrollContainer.scroll_vertical + 10) >= max_scroll && !_is_loading:
		# reach bottom
		_is_loading = true
		if _page >= 1 && _page < _total_pages:
			_page += 1
			_load_more()
		else:
			_is_loading = false
