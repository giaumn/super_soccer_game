extends Base

var MAX_WAIT_TIME = 15
var _elapsed = 0
var _confirmed = false
var _rejected = false

func _ready():
	$Center/Start/Label.text = 'START (' + var2str(MAX_WAIT_TIME - _elapsed) + ')'
	$Center/Status.hide()
	$Center/Home/Team.text = Local.home['team_name']
	$Center/Home/Coach.text = Local.home['user_name']
	$Center/Home/Flag.texture = load('res://assets/textures/w40/' + Local.home.country_code + '.png')
		
	$Center/Away/Team.text = Local.away['team_name']
	$Center/Away/Coach.text = Local.away['user_name']
	$Center/Away/Flag.texture = load('res://assets/textures/w40/' + Local.away.country_code + '.png')
	
	Match.connect(Constant.SIGNAL_MATCH_INITIALIZED, self, '_on_match_initialized')
	Match.connect(Constant.SIGNAL_MATCH_CANCELED, self, '_on_match_canceled')
	
	Sound.play('found.wav', 1)

func _on_Timer_timeout():
	_elapsed += 1
	$Center/Start/Label.text = 'START (' + var2str(MAX_WAIT_TIME - _elapsed) + ')'
	if _elapsed == MAX_WAIT_TIME:
		# rejecting after wait too long
		$Timer.stop()
		if _confirmed:
			get_tree().change_scene('res://scenes/Search.tscn')
		else:
			Local.requesting = false
			get_tree().change_scene('res://scenes/Main.tscn')

func _on_Start_button_up():
	_confirmed = true
	Match.confirm()
	$Center/Start.hide()
	$Center/Cancel.hide()
	$Center/Status.show()

func _on_match_initialized(data):
	Local.home = data.get('home')
	Local.away = data.get('away')
	Local.wait_secs_before_started = data.get('wait_secs_before_started')
	if Local.user_id == Local.home.get('id'):
		Local.me = Local.home
		Local.opponent = Local.away
	else:
		Local.me = Local.away
		Local.opponent = Local.home
	Local.requesting = false
	Music.stop()
	get_tree().change_scene('res://scenes/Play.tscn')
	print('play')

func _on_match_canceled():
	$Timer.stop()
	if _rejected:
		Local.requesting = false
		get_tree().change_scene('res://scenes/Main.tscn')
	else:
		get_tree().change_scene('res://scenes/Search.tscn')

func _on_Cancel_button_up():
	_rejected = true
	Match.reject()
	Local.requesting = false
	$Timer.stop()
	get_tree().change_scene('res://scenes/Main.tscn')
