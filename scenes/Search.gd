extends Base

func _ready():
	Match.connect(Constant.SIGNAL_MATCH_FOUND, self, '_on_match_found')
	
	Local.reconnected = false
	
	if Local.requesting:
		return
	
	Local.requesting = true
	Match.request()
	
	Music.change('romantic-hip-hop-groove.mp3')

func _on_Cancel_button_up():
	Local.requesting = false
	Match.cancel_request()
	get_tree().change_scene('res://scenes/Main.tscn')

func _on_match_found(home, away):
	Local.home = home
	Local.away = away
	if Local.user_id == home.get('id'):
		Local.me = home
		Local.opponent = away
	else:
		Local.me = away
		Local.opponent = home
	get_tree().change_scene('res://scenes/Preview.tscn')
