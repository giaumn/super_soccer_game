extends Control

var FormationPicker = preload('res://controls/FormationPicker.tscn')
var Player = preload('res://controls/Player.tscn')
var Shooter = preload('res://controls/Shooter.tscn')
var Setting = preload('res://controls/Setting.tscn')

var _my_selection
var _opponent_selection
var _count_to_start_elapsed = 0
var _current_turn
var _match_elapsed_secs = 0
var _match_elapsed_real_time = 0
var _turn_elapsed = 0
var _ball_control = null
var _play_round = 0
var _turn_count = 0
var _my_score = 0
var _opponent_score = 0

var _match_secs_real_life = null # 3 minutes + 36 seconds in real life
var _match_secs_in_game = 5400 # 90 minutes in game
var _turn_limit_secs = null
var _wait_secs_before_started = null # wait for 10s before starting the game
var _playing = false
var _current_player
var _prev_player
var _ball_control_player
var _prev_opponent_player
var _shooter
var _formation_picker
var _breaking = false
var _is_updating = false
var _ended = false
var _commenting = false

var _scale_map = {
	'me': [1, 1, 0.9, 0.8],
	'opponent': [0.8, 0.8, 0.9, 1]
}
var _position_map = {
	'me': {
		'0': Vector2(324, 570),
		'1': {
			'6': [
				Vector2(66, 386),
				Vector2(166, 386),
				Vector2(270, 386),
				Vector2(373, 386),
				Vector2(476, 386),
				Vector2(578, 386),
			],
			'5': [
				Vector2(84, 386),
				Vector2(203, 386),
				Vector2(324, 386),
				Vector2(443, 386),
				Vector2(561, 386),
			],
			'4': [
				Vector2(116, 386),
				Vector2(252, 386),
				Vector2(395, 386),
				Vector2(529, 386),
			],
			'3': [
				Vector2(140, 386),
				Vector2(323, 386),
				Vector2(505, 386),
			],
			'2': [
				Vector2(204, 386),
				Vector2(441, 386),
			],
		},
		'2': {
			'6': [
				Vector2(100, 220),
				Vector2(189, 220),
				Vector2(279, 220),
				Vector2(370, 220),
				Vector2(460, 220),
				Vector2(551, 220),
			],
			'5': [
				Vector2(109, 220),
				Vector2(214, 220),
				Vector2(323, 220),
				Vector2(431, 220),
				Vector2(535, 220),
			],
			'4': [
				Vector2(147, 220),
				Vector2(264, 220),
				Vector2(382, 220),
				Vector2(498, 220),
			],
			'3': [
				Vector2(174, 220),
				Vector2(323, 220),
				Vector2(470, 220),
			],
			'2': [
				Vector2(212, 220),
				Vector2(432, 220),
			],
		},
		'3': {
			'6': [
				Vector2(113, 38),
				Vector2(199, 38),
				Vector2(284, 38),
				Vector2(369, 38),
				Vector2(453, 38),
				Vector2(535, 38),
			],
			'5': [
				Vector2(131, 38),
				Vector2(226, 38),
				Vector2(324, 38),
				Vector2(422, 38),
				Vector2(520, 38),
			],
			'4': [
				Vector2(160, 38),
				Vector2(270, 38),
				Vector2(380, 38),
				Vector2(488, 38),
			],
			'3': [
				Vector2(209, 38),
				Vector2(326, 38),
				Vector2(440, 38),
			],
			'2': [
				Vector2(228, 38),
				Vector2(420, 38),
			],
		}
	},
	'opponent': {
		'0': Vector2(325, -18),
		'1': {
			'6': [
				Vector2(112, 135),
				Vector2(197, 135),
				Vector2(282, 135),
				Vector2(368, 135),
				Vector2(452, 135),
				Vector2(535, 135),
			],
			'5': [
				Vector2(141, 135),
				Vector2(233, 135),
				Vector2(325, 135),
				Vector2(417, 135),
				Vector2(507, 135),
			],
			'4': [
				Vector2(149, 135),
				Vector2(264, 135),
				Vector2(385, 135),
				Vector2(500, 135),
			],
			'3': [
				Vector2(189, 135),
				Vector2(325, 135),
				Vector2(459, 135),
			],
			'2': [
				Vector2(223, 135),
				Vector2(418, 135),
			],
		},
		'2': {
			'6': [
				Vector2(99, 305),
				Vector2(191, 305),
				Vector2(282, 305),
				Vector2(372, 305),
				Vector2(462, 305),
				Vector2(550, 305),
			],
			'5': [
				Vector2(121, 305),
				Vector2(223, 305),
				Vector2(325, 305),
				Vector2(429, 305),
				Vector2(532, 305),
			],
			'4': [
				Vector2(149, 305),
				Vector2(263, 305),
				Vector2(381, 305),
				Vector2(496, 305),
			],
			'3': [
				Vector2(178, 305),
				Vector2(324, 305),
				Vector2(470, 305),
			],
			'2': [
				Vector2(216, 305),
				Vector2(430, 305),
			],
		},
		'3': {
			'6': [
				Vector2(67, 495),
				Vector2(168, 495),
				Vector2(270, 495),
				Vector2(373, 495),
				Vector2(495, 495),
				Vector2(577, 495),
			],
			'5': [
				Vector2(99, 495),
				Vector2(213, 495),
				Vector2(323, 495),
				Vector2(435, 495),
				Vector2(546, 495),
			],
			'4': [
				Vector2(115, 495),
				Vector2(254, 495),
				Vector2(394, 495),
				Vector2(530, 495),
			],
			'3': [
				Vector2(170, 495),
				Vector2(323, 495),
				Vector2(474, 495),
			],
			'2': [
				Vector2(211, 495),
				Vector2(434, 495),
			],
		}
	}
}
var _line_players = {
	0: [],
	1: [],
	2: [],
	3: []
}
var _players = {}

func _ready():
	_formation_picker = FormationPicker.instance()
	add_child(_formation_picker)
	
	_formation_picker.anchor_left = 0.5
	_formation_picker.anchor_top = 0.5
	_formation_picker.anchor_right = 0.5
	_formation_picker.anchor_bottom = 0.5
	
	_formation_picker.rect_size = Vector2(186, 58)
	_formation_picker.rect_position = Vector2(32, 1100)
	_formation_picker.connect(Constant.SIGNAL_FORMATION_CHANGED, self, '_on_formation_changed')
	_formation_picker.set_formation(Local.me.get('formation'))
	
	_wait_secs_before_started = Local.wait_secs_before_started if Local.wait_secs_before_started > 0 else _wait_secs_before_started
	$Center/MyTurn.hide()
	$Center/OpponentTurn.hide()
	$Center/Status.text = var2str(_wait_secs_before_started)
	$Action.hide()
	
	$ScoreBoard/Home.text = 'FC. ' + Local.me.get('team_name')
	$ScoreBoard/HomeFlag.texture = load('res://assets/textures/w40/' + Local.me.get('country_code') + '.png')
	$ScoreBoard/Away.text = 'FC. ' + Local.opponent.get('team_name')
	$ScoreBoard/AwayFlag.texture = load('res://assets/textures/w40/' + Local.opponent.get('country_code') + '.png')
	
	if Local.me.get('id') == Local.home.get('id'):
		$Home/Name/Label.text = Local.me.get('user_name')
		$Home/Side/Label.text = 'HOME'
		$Away/Name/Label.text = Local.opponent.get('user_name')
		$Away/Side/Label.text = 'AWAY'
	else:
		$Home/Name/Label.text = Local.me.get('user_name')
		$Home/Side/Label.text = 'AWAY'
		$Away/Name/Label.text = Local.opponent.get('user_name')
		$Away/Side/Label.text = 'HOME'
	
	Music.change('soccer-stadium.mp3')
	
	if Local.reconnected:
		$Center/Title.hide()
		$Center/Status.hide()
		$Center/Sync.show()
	else:
		$Center/Status.show()
		$Center/Title.show()
		yield(get_tree().create_timer(1.5), 'timeout')
		$Timer.start()
		$Center/Sync.hide()
		$Commentary/Content.text = 'Preparing match. Set your favorite formation. You can only change formation at break time.'

	Match.connect(Constant.SIGNAL_MATCH_STARTED, self, '_on_match_started')
	Match.connect(Constant.SIGNAL_MATCH_NEW_TURN, self, '_on_new_turn')
	Match.connect(Constant.SIGNAL_MATCH_SKIP_TURN, self, '_on_skip_turn')
	Match.connect(Constant.SIGNAL_MATCH_NEW_ROUND, self, '_on_new_round')
	Match.connect(Constant.SIGNAL_MATCH_END_ROUND, self, '_on_end_round')
	Match.connect(Constant.SIGNAL_MATCH_SCORE, self, '_on_score')
	Match.connect(Constant.SIGNAL_MATCH_SYNC, self, '_on_sync')
	Match.connect(Constant.SIGNAL_MATCH_BREAK, self, '_on_break')
	Match.connect(Constant.SIGNAL_MATCH_RETURN, self, '_on_return')
	Match.connect(Constant.SIGNAL_MATCH_END, self, '_on_end')
	Match.connect(Constant.SIGNAL_MATCH_OPPONENT_SELECT, self, '_on_opponent_select')
	Match.connect(Constant.SIGNAL_MATCH_OPPONENT_CONFIRM, self, '_on_opponent_confirm')

func _on_formation_changed(formation):
	Local.formation = formation
	Match.change_formation(formation)

func _on_Timer_timeout():
	if _playing:
		_turn_elapsed += 1
		if _turn_elapsed > _turn_limit_secs:
			$Timer.stop()
			if _current_turn == Local.me.get('id'):
				$Center/MyTurn.text = 'Turn ended'
			else:
				$Center/OpponentTurn.text = 'Turn ended'
		else:
			if _current_turn == Local.me.get('id'):
				$Center/MyTurn.text = 'Your turn (' + var2str(_turn_limit_secs - _turn_elapsed) + ')'
				$Center/MyTurn.show()
			else:
				$Center/OpponentTurn.text = 'Opponent\'s turn (' + var2str(_turn_limit_secs - _turn_elapsed) + ')'
				$Center/OpponentTurn.show()
	else:
		_count_to_start_elapsed += 1
		$Center/Status.text = var2str(_wait_secs_before_started - _count_to_start_elapsed)
		if _count_to_start_elapsed > _wait_secs_before_started:
			Sound.play('kickoff.wav')
			$Center/Title.hide()
			$Center/Status.text = 'Kick off!'
			$Timer.stop()
		else:
			Sound.play('tick.wav')

func _on_DelayTimer_timeout():
	_count_to_start_elapsed += 1
	var remaining_secs = _wait_secs_before_started - _count_to_start_elapsed
	remaining_secs = remaining_secs if remaining_secs >= 0 else 0
	$Center/Status.text = var2str(remaining_secs)
	if _count_to_start_elapsed > _wait_secs_before_started:
		$DelayTimer.stop()
		_commenting = false
	else:
		if _breaking || _ended || _commenting:
			return
		Sound.play('tick.wav')
			
func _on_PassedTimer_timeout():
	_match_elapsed_secs += $PassedTimer.wait_time
	
	if _match_elapsed_secs == _match_secs_real_life / 2:
		$PassedTimer.stop()
	
	if _match_elapsed_secs >= _match_secs_real_life:
		$PassedTimer.stop()
		
	_update_match_time()
	
func _on_match_started(data):
	_current_player = 1
	_turn_elapsed = 0
	
	_update_match(data, false)
	$Center/Title.hide()
	$Center/Status.hide()
	$PassedTimer.start()
	
func _on_new_turn(data):
	if _breaking || !_playing || _ended:
		return

	_turn_elapsed = 0
	_update_match(data, false)
	
	$Center/MyTurn.show()
	$Center/OpponentTurn.show()
	
func _on_skip_turn(data):
	_sync_data(data)
	
	if _breaking || !_playing || _ended:
		return
	
	if _ball_control == Local.me.get('id'):
		# I'm attacking
		if _current_turn == Local.me.get('id'):
			# But i skipped
			_clear_formation()
			$Center/Status.show()
			$Center/Title.show()
			$Center/Status.text = '3'
			$Center/Title.text = 'You lost the ball'
			_count_to_start_elapsed = 0
			_wait_secs_before_started = 3
			_commenting = true
			$DelayTimer.start()
			$Action.disabled = true
			$Commentary/Content.text = 'You\'ve not made your move on time and lost the ball to your opponent. Your opponent will attack now.'
			Sound.play('lost_ball.wav')
		else:
			if _play_round < 3:
				$Commentary/Content.text = 'Your opponent has not made the move on time, you have successfully passed the ball to the chosen player.'
			else:
				_clear_formation()
				$Center/Status.show()
				$Center/Title.show()
				$Commentary/Content.text = 'Your opponent has not made the move on time, you have scored a new goal.'
				$Center/Title.text = 'Goal! Goal! Goal!'
				$Center/Status.text = _get_new_score(data)
				Sound.play('cheer.wav')
	else:
		# my opponent is attacking
		if _current_turn == Local.me.get('id'):
			# but I didn't play
			Sound.play('lost_ball.wav')
			if _play_round < 3:
				$Commentary/Content.text = 'You have not played on your turn, so your opponent has successfully passed the ball and moving forward.'
			else:
				_clear_formation()
				$Center/Status.show()
				$Center/Title.show()
				$Commentary/Content.text = 'You have not played on your turn, so your opponent has has scored a new goal.'
				$Center/Title.text = 'New score'
				$Center/Status.text = _get_new_score(data)
		else:
			# but my opponent skipped
			_clear_formation()
			$Center/Status.show()
			$Center/Title.show()
			$Center/Status.text = '3'
			$Center/Title.text = 'You will attack in'
			_count_to_start_elapsed = 0
			_wait_secs_before_started = 3
			_commenting = true
			$DelayTimer.start()
			$Commentary/Content.text = 'Your opponent was attacking but didn\'t play and lost the ball to you. You will attack now.'
			Sound.play('gain_ball.wav')
			
func _on_new_round(data):
	if _breaking || !_playing || _ended:
		return

	_my_selection = null
	_opponent_selection = null
	_turn_elapsed = 0
	_update_match(data, false)

func _on_end_round(data):
	if _breaking || !_playing:
		return
	
	_is_updating = true
	
	$Timer.stop()
	if _ball_control == Local.me.get('id'):
		if _current_turn == Local.opponent.get('id'):
			$Center/OpponentTurn.text = 'Turn ended'
	else:
		if _current_turn == Local.me.get('id'):
			$Center/MyTurn.text = 'Turn ended'
			$Action.disabled = true
			
	
	var home_selection = data.get('home_selection')
	var away_selection = data.get('away_selection')

	if home_selection == away_selection:
		# tackle success
		if _play_round < 3:
			_pass(home_selection, away_selection)
			
			yield(get_tree().create_timer(1), 'timeout')
			
			if _ball_control == Local.me.get('id'):
				# show me lose the ball
				$Center/Title.text = 'You lost the ball! Defend in'
				$Commentary/Content.text = 'Your opponent have successfully predict your move. So, you lost the ball...'
				Sound.play('lost_ball.wav')
			else:
				# show me gain the ball
				$Center/Title.text = 'Tackle success! Attack in'
				$Commentary/Content.text = 'You have successfully predict your opponent move. So, you have taken the ball...'
				Sound.play('gain_ball.wav')
		else:
			_shoot(home_selection, away_selection)
			
			yield(get_tree().create_timer(1.5), 'timeout')
			
			if _ball_control == Local.me.get('id'):
				# show me lose the ball
				$Center/Title.text = 'Strike failed! Defend in'
				$Commentary/Content.text = 'Your opponent have successfully predict your move. So, you\'ve lost the ball...'
				Sound.play('lost_ball.wav')
			else:
				# show me gain the ball
				$Center/Title.text = 'Excellent save! Attack in'
				$Commentary/Content.text = 'You have successfully predict your opponent move. So, you\'ve taken the ball...'
				Sound.play('yay.wav')
				
		_clear_formation()
		$Center/Status.text = '3'
		_count_to_start_elapsed = 0
		_wait_secs_before_started = 3
		_commenting = true
		$DelayTimer.start()
		$Center/Status.show()
		$Center/Title.show()	
	else:
		# show the ball forward
		if _play_round < 3:
			_pass(home_selection, away_selection)
			yield(get_tree().create_timer(1), 'timeout')
		else:
			_shoot(home_selection, away_selection)
			
			yield(get_tree().create_timer(1.5), 'timeout')
			
			_clear_formation()
			
			if _ball_control == Local.me.get('id'):
				$Center/Title.text = 'Goal! Goal! Goal!'
				Sound.play('cheer.wav')
			else:
				$Center/Title.text = 'New score'
				Sound.play('lost_ball.wav')

			$Center/Status.text = _get_new_score(data)
			$Center/Status.show()
			$Center/Title.show()
	_is_updating = false
	
func _on_score(_data):
	pass

func _on_sync(data):
	$Center/Sync.hide()
	
	_turn_elapsed = data.get('turn_elapsed')
	_playing = true
	
	$PassedTimer.start()
	
	_match_elapsed_secs = data.get('elapsed')
	
	# update the match timer's time only after reconnected
	_update_match(data, true)

func _on_break(data):
	_breaking = true
	_playing = false
	
	while _is_updating:
		yield(get_tree().create_timer(1), 'timeout')
		continue
	
	Sound.stop_second()
	Sound.play('break.wav')
	
	_clear_formation()
	$Center/Status.show()
	$Center/Title.show()
	
	var break_secs = data.get('break_secs')
	$Center/Status.text = var2str(break_secs)
	$Center/Title.text = 'Break time!'
	
	_count_to_start_elapsed = 0
	_wait_secs_before_started = break_secs
	$DelayTimer.start()
	$Action.disabled = true
	$PassedTimer.stop()
	$Timer.stop()
	$Commentary/Content.text = 'You can change your formation before the match is continued.'
	$Center/OpponentTurn.text = 'Turn ended'
	$Center/MyTurn.text = 'Turn ended'
	
	_formation_picker.show()
	_match_elapsed_secs = _match_secs_real_life / 2
	_update_match_time()
	
func _on_return(_data):
	_breaking = false
	_clear_formation()
	$Center/Status.text = '3'
	$Center/Title.text = 'Second-half begin in'
	_count_to_start_elapsed = 0
	_wait_secs_before_started = 3
	$DelayTimer.start()
	$Action.disabled = true
	$Commentary/Content.text = 'You can change your formation before the match is continued.'
	_formation_picker.hide()
	yield(get_tree().create_timer(3.7), 'timeout')
	Sound.stop()
	Sound.stop_second()
	
func _on_end(data):
	_playing = false
	_ended = true
	
	while _is_updating:
		yield(get_tree().create_timer(1), 'timeout')
		continue
		
	_clear_formation()
	$Center/Status.show()
	$Center/Title.show()
	$Center/Status.text = '5'
	$Center/Title.text = 'Match ended'
	
	_count_to_start_elapsed = 0
	_wait_secs_before_started = 5
	$DelayTimer.start()
	$Action.disabled = true
	$Commentary/Content.text = 'Match ended. Waiting for result...'
	$Timer.stop()
	$PassedTimer.stop()
	
	yield(get_tree().create_timer(4.7), 'timeout')
	Sound.stop()
	Sound.stop_second()
	
	Local.current_match = data
	get_tree().change_scene('res://scenes/Result.tscn')
	
func _on_opponent_select(turn_count, selection):
	if turn_count != _turn_count:
		return
		
	if _ball_control == Local.me.get('id'):
		if _current_turn == Local.opponent.get('id'):
			var player = _players.get(selection)
			if player != null:
				player.opponent_select(_prev_opponent_player)
				_prev_opponent_player = player
	
func _on_opponent_confirm(turn_count, selection):
	if turn_count != _turn_count:
		return

	if _ball_control == Local.me.get('id'):
		if _current_turn == Local.opponent.get('id'):
			_opponent_selection = selection
			if _play_round < 3:
				var player = _players.get(selection)
				if player != null:
					player.opponent_lock()

func _on_Action_button_up():
	if _my_selection == null:
		Modal.error('You have not choose your move!')
	else:
		Match.move(_turn_count, _my_selection)
		if _current_turn == Local.me.get('id'):
			if _play_round < 3:
				var player = _players.get(_my_selection)
				if player != null:
					player.lock()
			else:
				if _shooter != null:
					_shooter.lock()
					
func _update_match(data, is_sync):
	_playing = true
	
	_sync_data(data)
	_update_match_time()
	
	_formation_picker.hide()
	_prev_player = null
	_prev_opponent_player = null
	$Action.show()
	$ScoreBoard/HomeScore.text = var2str(_my_score)
	$ScoreBoard/AwayScore.text = var2str(_opponent_score)
	$Center/Status.hide()
	$Center/Title.hide()
	$Center/Sync.hide()
	
	if Local.league == null:
		$LeagueName.text = 'International match'
		$LeagueLevel.text = 'Win +3    Draw +1    Lose 0'
		$LeagueRound.hide()
		$LeagueLevel.show()
		$LeagueName.show()
	else:
		var current_round = Local.league.get('current_round')
		if current_round == null: 
			$LeagueRound.hide()
		else:
			$LeagueRound.show()
			$LeagueRound.text = 'Round ' + var2str(current_round.get('round_number'))
		$LeagueLevel.text = Local.league.get('level')
		$LeagueName.text = Local.league.get('name')
		$LeagueRound.show()
		$LeagueLevel.show()
		$LeagueName.show()
	
	Sound.stop_second()
	
	if _play_round < 3:
		$Goal.hide()
	else:
		$Goal.show()
	
	if _current_turn == Local.me.get('id'):
		if is_sync:
			var remaining_secs = _turn_limit_secs - _turn_elapsed if _turn_limit_secs >= _turn_elapsed else _turn_limit_secs
			$Center/MyTurn.text = 'Your turn (' + var2str(remaining_secs) + ')'
		else:
			$Center/MyTurn.text = 'Your turn (' + var2str(_turn_limit_secs) + ')'
		$Center/OpponentTurn.text = 'Turn ended'
		$Action.disabled = false
		$Center/MyTurn.show()
	else:
		$Center/MyTurn.text = 'Turn ended'
		if is_sync:
			var remaining_secs = _turn_limit_secs - _turn_elapsed if _turn_limit_secs >= _turn_elapsed else _turn_limit_secs
			$Center/OpponentTurn.text = 'Opponent\'s turn (' + var2str(remaining_secs) + ')'
		else:
			$Center/OpponentTurn.text = 'Opponent\'s turn (' + var2str(_turn_limit_secs) + ')'
		$Action.disabled = true
		$Center/OpponentTurn.show()
	
	if _ball_control == Local.me.get('id'):
		if _current_turn == Local.me.get('id'):
			# it's my turn, so populate my formation on screen
			if _play_round < 3:
				$Commentary/Content.text = 'Choose a player to pass the ball to and lock your choice.'
				_load_formation(Constant.ME, Local.me.get('formation'))
			else:
				$Commentary/Content.text = 'Now, you\'re facing the goal keeper. Choose where you want to strike...'
				_load_shooter(Constant.ATTACK)
		else:
			$Commentary/Content.text = 'You\'ve made your selection. Now, wait for your opponent to predict your move!'
	else:
		if _current_turn == Local.me.get('id'):
			if _play_round < 3:
				$Commentary/Content.text = 'Predict the player which your opponent have choose to pass the ball to...'
			else:
				$Commentary/Content.text = 'Your opponent has made their way to your goal and about to strike. Choose wisely to catch the ball...'
				_load_shooter(Constant.DEFEND)
		else:
			$Commentary/Content.text = 'Your opponent is thinking! Just wait...'
			# it's my opponent's turn, so populate my opponent's formation on screen
			if _play_round < 3:
				_load_formation(Constant.OPPONENT, Local.opponent.get('formation'))
			else:
				_load_shooter(Constant.IDLE)
				
	# normalize current line of players if not goal keeper
	if _play_round >= 1 && _play_round <= 3:
		var current_line_of_players = _line_players.get(_play_round)
		if current_line_of_players != null:
			for player_number in current_line_of_players:
				var player = _players.get(player_number)
				if player != null && player.is_inside_tree():
					player.normalize()
				
	# highlight selectable players
	if _play_round < 3 && _current_turn == Local.me.get('id'):
		var selectable_line_of_players = _line_players.get(_play_round + 1)
		if selectable_line_of_players != null:
			for player_number in selectable_line_of_players:
				var player = _players.get(player_number)
				if player != null:
					player.highlight()
	
	if _breaking || _ended:
		return
		
	$Timer.start()
	Sound.play_second('start_turn.wav')

func _sync_data(data):
	Local.home = data.get('home')
	Local.away = data.get('away')
	if Local.user_id == Local.home.get('id'):
		Local.me = Local.home
		Local.opponent = Local.away
	else:
		Local.me = Local.away
		Local.opponent = Local.home
	_match_secs_real_life = data.get('match_secs')
	_turn_limit_secs = data.get('turn_limit_secs')
	_current_turn = data.get('current_turn')
	_current_player = data.get('current_player')
	_ball_control = data.get('ball_control')
	_play_round = data.get('play_round')
	_match_elapsed_real_time = data.get('elapsed')
	_turn_count = data.get('turn_count')

	if Local.home.get('id') == Local.me.get('id'):
		_my_score = data.get('home_score')
		_opponent_score = data.get('away_score')
	else:
		_my_score = data.get('away_score')
		_opponent_score = data.get('home_score')
	
func _update_match_time():
	var match_elapsed_secs_in_game = _match_elapsed_secs * _match_secs_in_game / _match_secs_real_life
	var seconds = int(match_elapsed_secs_in_game) % 60
	# warning-ignore:integer_division
	var minutes = int(match_elapsed_secs_in_game) / 60
	
	if minutes == 45:
		seconds = 0
		
	$ScoreBoard/Time/Label.text = '%02d:%02d' % [minutes, seconds]

func _pass(home_selection, away_selection):
	var to_player
	if Local.home.get('id') == _ball_control:
		to_player = _players.get(home_selection)
	else:
		to_player = _players.get(away_selection)
	
	if to_player != null:
		_ball_control_player.pass_ball(to_player)

func _shoot(home_selection, away_selection):
	if _shooter != null:
		if _ball_control == Local.home.get('id'):
			_shooter.shoot(home_selection, away_selection)
		else:
			_shooter.shoot(away_selection, home_selection)

func _get_new_score(data):
	var score
	if data.get('ball_control') == Local.me.get('id'):
		if Local.home.get('id') == Local.me.get('id'):
			score = var2str(data.get('home_score') + 1) + ' - ' + var2str(data.get('away_score'))
		else:
			score = var2str(data.get('away_score') + 1) + ' - ' + var2str(data.get('home_score'))
	else:
		if Local.home.get('id') == Local.me.get('id'):
			score = var2str(data.get('home_score')) + ' - ' + var2str(data.get('away_score') + 1)
		else:
			score = var2str(data.get('away_score')) + ' - ' + var2str(data.get('home_score') + 1)
	return score
	
func _load_formation(side, formation):
	var formation_lines = formation.split_floats('-', false)
	var player_number = 1
	
	_clear_formation()

	var is_having_ball = _current_player == player_number
	_load_player(_position_map[side]['0'], _scale_map[side][0], side, player_number, is_having_ball) # GK
	
	var index = 1
	for line in formation_lines:
		var line_player_count = int(line)
		var scale = _scale_map[side][index]
		var position = _position_map[side][var2str(index)][var2str(int(line))]
		for player_index in line_player_count:
			player_number += 1
			_line_players[index].push_back(player_number)
			
			is_having_ball = _current_player == player_number
			_load_player(position[player_index], scale, side, player_number, is_having_ball)
		index += 1
	
func _load_player(position, scale, side, number, is_having_ball):
	var player = Player.instance()
	player.init(side, number, is_having_ball)
	player.rect_scale = Vector2(scale, scale)
	player.rect_position = position
	player.rect_size = Vector2(74, 71)
	player.connect(Constant.SIGNAL_GAME_PLAYER_SELECTED, self, '_on_player_selected')
	if is_having_ball:
		_ball_control_player = player
	$Formation.add_child(player)
	_players[player.number] = player
	return player
	
func _clear_formation():
	_clear_shooter()
	_players = {}
	for n in $Formation.get_children():
		$Formation.remove_child(n)
		n.queue_free()

func _on_player_selected(player):
	if _current_turn == Local.opponent.get('id'):
		return
		
	# I'm controling the ball, check if the player clicked is able to be selected as next player
	var next_line_players = _line_players.get(_play_round + 1)
	if next_line_players != null && next_line_players.has(player.number):
		# only change direction if it's me who is attacking
		player.select(_prev_player, _ball_control_player, _ball_control == Local.me.get('id'))
		_my_selection = player.number
		_prev_player = player
		
		if _ball_control == Local.opponent.get('id'):
			Match.select(_turn_count, player.number)

func _load_shooter(action):
	_clear_formation()
	_shooter = Shooter.instance()
	_shooter.init(action)
	_shooter.rect_position = Vector2(129, 33)
	_shooter.connect(Constant.SIGNAL_GAME_SHOOTER_SELECTED, self, '_on_shooter_selected')
	$Goal.add_child(_shooter)
	
func _clear_shooter():
	if _shooter == null:
		return
	$Goal.remove_child(_shooter)
	_shooter = null

func _on_shooter_selected(selection):
	_my_selection = selection

func _on_Quit_button_up():
	var _modal = Modal.confirm('Quit?', 'You will lose this match, still quit?')
	_modal.connect(Constant.SIGNAL_MODAL_YES, self, '_on_modal_yes')

func _on_modal_yes():
	Match.quit()

func _on_Setting_button_up():
	var setting = Setting.instance()
	add_child(setting)
