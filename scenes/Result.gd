extends Base

func _ready():
	var home_score = Local.current_match.get('home_score')
	var away_score = Local.current_match.get('away_score')
	
	$ScoreBoard/Home.text = Local.me.get('team_name')
	$ScoreBoard/HomeFlag.texture = load('res://assets/textures/w40/' + Local.me.get('country_code') + '.png')
	$ScoreBoard/Away.text = Local.opponent.get('team_name')
	$ScoreBoard/AwayFlag.texture = load('res://assets/textures/w40/' + Local.opponent.get('country_code') + '.png')
	$Center/Panel/LeftCoach.text = Local.me.get('user_name')
	$Center/Panel/RightCoach.text = Local.opponent.get('user_name')
	
	var my_score = 0
	var opponent_score = 0
	
	if Local.me.get('id') == Local.home.get('id'):
		my_score = home_score
		opponent_score = away_score
		$Rewards/Value.text = 'Reward +' + var2str(int(Local.current_match.get('home_rewards'))) + '$'
	else:
		my_score = away_score
		opponent_score = home_score
		$Rewards/Value.text = 'Reward +' + var2str(int(Local.current_match.get('away_rewards'))) + '$'
		
	var user_quit = Local.current_match.get('user_quit')
	if user_quit == null:
		if home_score > away_score:
			if Local.me.get('id') == Local.home.get('id'):
				Music.change('crowd-cheer.mp3')
				$Center/Status.text = 'You win'
			else:
				$Center/Status.text = 'You lose'
		elif home_score < away_score:
			if Local.me.get('id') == Local.home.get('id'):
				$Center/Status.text = 'You lose'
			else:
				Music.change('crowd-cheer.mp3')
				$Center/Status.text = 'You win'
		else:
			my_score = home_score
			opponent_score = home_score
			$Center/Status.text = 'Draw'
	else:
		if user_quit == Local.me.get('id'):
			$Center/Status.text = 'You lose'
		else:
			Music.change('crowd-cheer.mp3')
			$Center/Status.text = 'You win'

	$ScoreBoard/HomeScore.text = var2str(my_score)
	$ScoreBoard/AwayScore.text = var2str(opponent_score)
	
	var total_possession = Local.current_match.get('home_possession') + Local.current_match.get('away_possession')
	if Local.me.get('id') == Local.home.get('id'):
		var my_possession = int(ceil(Local.current_match.get('home_possession') / total_possession * 100)) if total_possession > 0 else 0
		$Center/Panel/LeftPasses.text = var2str(Local.current_match.get('home_passes'))
		$Center/Panel/LeftShots.text = var2str(Local.current_match.get('home_shots'))
		$Center/Panel/LeftTackles.text = var2str(Local.current_match.get('home_tackles'))
		$Center/Panel/LeftGoalSaves.text = var2str(Local.current_match.get('home_goal_saves'))
		$Center/Panel/LeftPossession.text = var2str(my_possession) + '%'
		
		$Center/Panel/RightPasses.text = var2str(Local.current_match.get('away_passes'))
		$Center/Panel/RightShots.text = var2str(Local.current_match.get('away_shots'))
		$Center/Panel/RightTackles.text = var2str(Local.current_match.get('away_tackles'))
		$Center/Panel/RightGoalSaves.text = var2str(Local.current_match.get('away_goal_saves'))
		$Center/Panel/RightPossession.text = var2str(100 - my_possession) + '%'
	else:
		var my_possession = int(ceil(Local.current_match.get('away_possession') / total_possession * 100)) if total_possession > 0 else 0
		$Center/Panel/LeftPasses.text = var2str(Local.current_match.get('away_passes'))
		$Center/Panel/LeftShots.text = var2str(Local.current_match.get('away_shots'))
		$Center/Panel/LeftTackles.text = var2str(Local.current_match.get('away_tackles'))
		$Center/Panel/LeftGoalSaves.text = var2str(Local.current_match.get('away_goal_saves'))
		$Center/Panel/LeftPossession.text = var2str(my_possession) + '%'
		
		$Center/Panel/RightPasses.text = var2str(Local.current_match.get('home_passes'))
		$Center/Panel/RightShots.text = var2str(Local.current_match.get('home_shots'))
		$Center/Panel/RightTackles.text = var2str(Local.current_match.get('home_tackles'))
		$Center/Panel/RightGoalSaves.text = var2str(Local.current_match.get('home_goal_saves'))
		$Center/Panel/RightPossession.text = var2str(100 - my_possession) + '%'

func _on_Quit_button_up():
	get_tree().change_scene('res://scenes/Main.tscn')

func _on_Play_button_up():
	get_tree().change_scene('res://scenes/Search.tscn')

func _on_Board_button_up():
	get_tree().change_scene('res://scenes/Board.tscn')
