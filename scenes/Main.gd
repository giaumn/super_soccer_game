extends Base

var Profile = preload('res://controls/Profile.tscn')
var Setting = preload('res://controls/Setting.tscn')

func _ready():
	$ProgressBar.hide()
	User.connect(Constant.SIGNAL_USER_HAD_INFO, self, '_on_user_had_info', [], 8)
	if Server.connected:
		User.fetch()
		$UserInfo/Coach/Label.text = Local.user_name
		$UserInfo/Team/Label.text = 'FC. ' + Local.team_name
		$UserInfo/Flag.texture = load('res://assets/textures/w40/' + Local.country_code + '.png')
		Music.start()
		Music.change('happy-light-childish-jumpy-pleased-positive-music.mp3')
		$Buttons/Play/Icon/AnimationPlayer.play('rotate')
		$Buttons/Board/AnimationPlayer.play('resize')
		$Center/AnimationPlayer.play('rotate')
	else:
		_disable_elements()
		$ProgressBar.show()
		Server.initialize()

	var music_volume = Storage.get_item('music_volume')
	var sound_volume = Storage.get_item('sound_volume')
	
	music_volume = music_volume if music_volume != null else 0
	sound_volume = sound_volume if sound_volume != null else 0

	Music.change_volume(music_volume)
	Sound.change_volume(sound_volume)
	
func _on_error(error_code, message):
	._on_error(error_code, message)
	if error_code < ErrorCode.SERVER_ERROR_MIN_CODE:
		_disable_elements()
		if error_code == ErrorCode.SERVER_DISCONNECTED:
			$ProgressBar/Status.text = 'Reconnecting to server...'
			$ProgressBar/TextureProgress.value = 0
			$ProgressBar.show()
			$ProgressBar/Timer.start()

func _on_Timer_timeout():
	if $ProgressBar/TextureProgress.value <= 60:
		$ProgressBar/TextureProgress.value += 10
	
func _on_user_had_info(user_info):
	._on_user_had_info(user_info)
	FBA.setUserId(var2str(Local.user_id))
	$UserInfo/Coach/Label.text = user_info.get('name')
	$UserInfo/Team/Label.text = 'FC. ' + user_info.get('team_name')
	$UserInfo/Flag.texture = load('res://assets/textures/w40/' + user_info['country_code'] + '.png')
	$Buttons/Play.disabled = false
	$Buttons/Board.disabled = false
	$Setting.disabled = false
	$Edit.disabled = false
	$Money/Label.text = user_info.get('cash')
	$ProgressBar/TextureProgress.value = 100
	$ProgressBar/Status.text = 'Server connected.'
	$ProgressBar/Timer.stop()
	$LoadedDelayTimer.start()
	
	var national_league_unlocked_at = user_info.get('national_league_unlocked_at')
	if national_league_unlocked_at == null:
		var days_played_in_last_7_days = user_info.get('days_played_in_last_7_days')
		days_played_in_last_7_days = 0 if days_played_in_last_7_days == null else days_played_in_last_7_days
		$NationLeague.disabled = true
		$NationLeague/Subtitle.show()
		$NationLeague/Name.show()
		$NationLeague/Lock.show()
		$NationLeague/UnlockProgress.show()
		$NationLeague/CurrentCount.hide()
		$NationLeague/Joined.hide()
		$NationLeague/MaxCount.hide()
		$NationLeague/CurrentCount.hide()
		$NationLeague/Right.hide()
		$NationLeague/Circle.hide()
		$NationLeague/Season.hide()
		$NationLeague/UnlockProgress.text = var2str(int(days_played_in_last_7_days)) + '/7'
	else:
		$NationLeague/Lock.hide()
		$NationLeague/UnlockProgress.hide()
		$NationLeague/Subtitle.show()
		$NationLeague/Name.show()
		var national_league = user_info.get('national_league')
		if national_league == null:
			# waiting for new federation established
			$NationLeague.disabled = true
			$NationLeague/Subtitle.text = 'Waiting for other players...'
			$NationLeague/Joined.show()
			$NationLeague/CurrentCount.show()
			$NationLeague/MaxCount.show()
			$NationLeague/Circle.hide()
			$NationLeague/Right.hide()
			$NationLeague/Season.hide()
			$NationLeague/CurrentCount.text = var2str(int(user_info.get('idle_count')))
			$NationLeague/MaxCount.text = var2str(int(user_info.get('max_idle_count'))) + ' MAX'
		else:
			# federation & leagues were created
			var started_at = national_league.get('started_at')
			if started_at == null:
				# national league not started
				$NationLeague.disabled = true
				$NationLeague/Joined.show()
				$NationLeague/CurrentCount.show()
				$NationLeague/MaxCount.show()
				$NationLeague/Circle.hide()
				$NationLeague/Right.hide()
				$NationLeague/Season.hide()
				$NationLeague/CurrentCount.text = var2str(int(national_league.get('current_team_count')))
				$NationLeague/MaxCount.text = var2str(int(national_league.get('max_team_count'))) + ' MAX'
				
				var starts_in = national_league.get('starts_in')
				if starts_in == 0:
					$NationLeague/Subtitle.text = 'Season starts today'
				else:
					$NationLeague/Subtitle.text = 'Season starts in ' + var2str(int(starts_in)) + ' day(s)'
			else:
				var ended_at = national_league.get('ended_at')
				if ended_at == null:
					# in progress
					$NationLeague.disabled = false
					$NationLeague/Joined.hide()
					$NationLeague/CurrentCount.hide()
					$NationLeague/MaxCount.hide()
					$NationLeague/Circle.hide()
					$NationLeague/Season.hide()
					$NationLeague/CurrentPosition.show()
					$NationLeague/Right.show()
					var current_round = national_league.get('current_round')
					var current_round_number = int(current_round.get('round_number'))
					var my_position = user_info.get('my_position')
					$NationLeague/Subtitle.text = 'ROUND ' + var2str(current_round_number) + ', ' + current_round.get('formatted_start_date') + ' ' + current_round.get('start_time')
					$NationLeague/CurrentPosition.text = '?' if my_position == null else var2str(int(my_position))
				else:
					# ended
					$NationLeague.disabled = false
					$NationLeague/Joined.hide()
					$NationLeague/CurrentCount.hide()
					$NationLeague/MaxCount.hide()
					$NationLeague/Circle.hide()
					$NationLeague/CurrentPosition.show()
					$NationLeague/Right.hide()
					$NationLeague/Season.show()
					$NationLeague/Season.text = national_league.get('season')
					var my_position = user_info.get('my_position')
					var ended_days = national_league.get('ended_days')
					if ended_days < 15:
						if ended_days == 0:
							$NationLeague/Subtitle.text = 'Season ended today'
						else:
							$NationLeague/Subtitle.text = 'Season ended ' + var2str(int(ended_days)) + ' ago'
					else:
						$NationLeague/Subtitle.text = 'New Season in ' + var2str(int(national_league.get('new_season_in_days'))) + ' day(s).'
					$NationLeague/CurrentPosition.text = '?' if my_position == null else var2str(int(my_position))
	
	var current_match = user_info.get('current_match')
	if current_match != null:
		# reconnect to match
		Match.reconnect()
		var home = current_match.get('home')
		var away = current_match.get('away')
		Local.wait_secs_before_started = current_match.get('wait_secs_before_started')
		Local.reconnected = true
		Local.home = home
		Local.away = away
		Local.league = current_match.get('league')
		if Local.user_id == home.get('id'):
			Local.me = home
			Local.opponent = away
		else:
			Local.me = away
			Local.opponent = home
		Music.stop()
		get_tree().change_scene('res://scenes/Play.tscn')
	else:
		Music.start()
		$Buttons/Play/Icon/AnimationPlayer.play('rotate')
		$Buttons/Board/AnimationPlayer.play('resize')
		$Center/AnimationPlayer.play('rotate')
		
func _on_LoadedDelayTimer_timeout():
	$ProgressBar.hide()
	$NationLeague.show()
	$Center/AnimationPlayer.play('rotate')

func _on_Play_button_up():
	if Local.last_cancelation != null && OS.get_unix_time() - Local.last_cancelation <= 15:
		Modal.error('You have just cancelled a match, please wait for a few moments. Thank you!')
		return
	get_tree().change_scene('res://scenes/Search.tscn')

func _disable_elements():
	$Buttons/Play.disabled = true
	$Buttons/Board.disabled = true
	$Setting.disabled = true
	$Edit.disabled = true
	$NationLeague.disabled = true

func _on_Board_button_up():
	get_tree().change_scene('res://scenes/Board.tscn')

func _on_Edit_button_up():
	var profile = Profile.instance()
	add_child(profile)
	profile.init(Local.team_name, Local.user_name)
	var data = yield(profile, Constant.SIGNAL_USER_UPDATED_SUCCESS)
	Local.team_name = data['team_name']
	Local.user_name = data['name']
	$UserInfo/Team/Label.text = 'FC. ' + Local.team_name
	$UserInfo/Coach/Label.text = Local.user_name

func _on_Setting_button_up():
	var setting = Setting.instance()
	add_child(setting)

func _on_NationLeague_button_up():
	get_tree().change_scene('res://scenes/NationalLeague.tscn')
