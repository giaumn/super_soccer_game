extends Base

var LeagueRound = preload('res://controls/LeagueRound.tscn')
var LeagueStandings = preload('res://controls/LeagueStandings.tscn')

func _ready():
	$TabContainer/Matches/Scroll.get_v_scrollbar().rect_scale.x = 0
	$TabContainer/Standings/Scroll.get_v_scrollbar().rect_scale.x = 0
	$TabContainer/Matches/Scroll.rect_size.y = $TabContainer/Matches.rect_size.y - 5
	$TabContainer/Matches/Scroll.rect_size.x = $TabContainer/Matches.rect_size.x - 5
	$TabContainer/Standings/Scroll.rect_size.y = $TabContainer/Standings.rect_size.y - 5
	$TabContainer/Standings/Scroll.rect_size.x = $TabContainer/Standings.rect_size.x - 5
	
	Loader.show(self)
	
	User.fetch_national_league()
	var national_league = yield(User, Constant.SIGNAL_HTTP_DATA)
	var country_code = national_league.get('country_code')
	var name = national_league.get('name')
	var level = national_league.get('level')
	
	$Title/Level.text = level
	$Title/Flag.texture = load('res://assets/textures/w40/' + country_code + '.png')
	$Title/Name.text = name
	
	var current_round_number = national_league.get('current_round')
	var rounds = national_league.get('rounds')
	for r in rounds:
		var round_matches = LeagueRound.instance()
		round_matches.init(r, current_round_number)
		$TabContainer/Matches/Scroll/Container.add_child(round_matches)
	
	var standings = national_league.get('standings')
	var league_standings = LeagueStandings.instance()
	league_standings.init(standings)
	$TabContainer/Standings/Scroll/Container.add_child(league_standings)
	
	Loader.hide(self)

	$Buttons.show()
	yield(get_tree().create_timer(0.2), 'timeout')
	$TabContainer/Matches/Scroll.get_v_scrollbar().value = (current_round_number - 1) * 790

func _on_Back_button_up():
	get_tree().change_scene('res://scenes/Main.tscn')

func _on_Matches_button_up():
	$Buttons/Matches.texture_normal = preload('res://assets/textures/rbb_white.png')
	$Buttons/Matches/Label.add_color_override('font_color', Color('545454'))
	$Buttons/Standings.texture_normal = null
	$Buttons/Standings/Label.add_color_override('font_color', Color('ffffff'))
	$TabContainer.current_tab = 0

func _on_Standings_button_up():
	$Buttons/Matches.texture_normal = null
	$Buttons/Matches/Label.add_color_override('font_color', Color('ffffff'))
	$Buttons/Standings.texture_normal = preload('res://assets/textures/rbb_white.png')
	$Buttons/Standings/Label.add_color_override('font_color', Color('545454'))
	$TabContainer.current_tab = 1
