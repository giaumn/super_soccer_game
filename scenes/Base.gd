extends Node

class_name Base

var _error_modal

func _ready():
	Server.connect(Constant.SIGNAL_ERROR, self, '_on_error')
	get_tree().get_root().connect('size_changed', self, '_on_size_changed')
	var s = get_tree().current_scene
	var file_name = s.filename.split('/')
	if file_name.size() > 0:
		var screen_class = file_name[file_name.size() - 1]
		if screen_class != '' && screen_class != null:
			var screen_name = screen_class.split('.')[0]
			FBA.screen(screen_name, screen_class)

func _on_error(_error_code, message):
	_error_modal = Modal.error(message)
	
func _on_user_had_info(_response):
	if _error_modal != null:
		_error_modal.close()

func _on_size_changed():
	pass
